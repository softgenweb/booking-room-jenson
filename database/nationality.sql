-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2019 at 09:45 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mw`
--

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `id` int(11) NOT NULL,
  `shortcode` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`id`, `shortcode`, `nationality`, `status`) VALUES
(1, 'AMS', 'AMSTERDAM', 1),
(2, 'AUS', 'AUSTRALIA', 1),
(3, 'B/TAN', 'BHTAN', 1),
(4, 'BAN', 'BANGLADESH', 1),
(5, 'BRU', 'BRUNEI', 1),
(6, 'CAM', 'CAMBODIAN', 1),
(7, 'CHINA', 'CHINA', 1),
(8, 'COL', 'COLOMBIA', 1),
(9, 'FRA', 'FRANCE', 1),
(10, 'GER', 'GERMANY', 1),
(11, 'INDIA', 'INDIA', 1),
(12, 'INDO`S', 'INDONESIA', 1),
(13, 'IRAN', 'IRAN', 1),
(14, 'ITA', 'ITALY', 1),
(15, 'JAP', 'JAPAN', 1),
(16, 'KOR', 'KOREA', 1),
(17, 'LEBA', 'LEBANON', 1),
(18, 'MAL', 'MALAYSIA', 1),
(19, 'MAU', 'MAURITIUS', 1),
(20, 'MYAN', 'MYANMAR', 1),
(21, 'OTHER', 'OTHER', 1),
(22, 'PAK', 'PAKISTAN', 1),
(23, 'PHIL', 'PHILIPPINES', 1),
(24, 'SAU', 'SAUDI ARABIA', 1),
(25, 'SING', 'SINGAPORE', 1),
(26, 'SL', 'SRI LANKA', 1),
(27, 'SYR', 'SYRIA', 1),
(28, 'THA', 'THAILAND', 1),
(29, 'UK', 'UNITED KINGDOM', 1),
(30, 'USA', 'UNITED STATES', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
