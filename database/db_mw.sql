-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2019 at 08:09 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mw`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `system_ip` varchar(200) NOT NULL,
  `activity` varchar(220) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `system_ip`, `activity`, `user_id`, `date_created`, `status`) VALUES
(1, '', 'Book Room(211) Created by admin', 1, '2019-07-29 17:45:14', 1),
(2, '', 'Book Room(507) Created by admin', 1, '2019-08-01 13:32:17', 1),
(3, '', 'Add Rate Created by admin', 1, '2019-08-20 13:18:41', 1),
(4, '', 'Book Room(203) Created by admin', 1, '2019-08-20 13:53:31', 1),
(5, '', 'Add Rate Created by admin', 1, '2019-08-31 19:10:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` bigint(100) NOT NULL,
  `booking_id` varchar(255) NOT NULL,
  `room_no` varchar(255) NOT NULL,
  `check_in` varchar(200) NOT NULL,
  `check_in_time` varchar(200) NOT NULL,
  `cheak_out` varchar(200) NOT NULL,
  `cheak_out_time` varchar(200) NOT NULL,
  `checked_in` varchar(200) NOT NULL,
  `checked_in_time` varchar(200) NOT NULL,
  `cheaked_out` varchar(200) NOT NULL,
  `cheaked_out_time` varchar(200) NOT NULL,
  `rate_type_id` int(100) NOT NULL,
  `custom_rate` varchar(255) NOT NULL,
  `deposit_amt` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `membership` varchar(255) NOT NULL,
  `remark` text NOT NULL,
  `passport_ic` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `trace` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date_created` varchar(255) NOT NULL,
  `date_modified` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `booking_id`, `room_no`, `check_in`, `check_in_time`, `cheak_out`, `cheak_out_time`, `checked_in`, `checked_in_time`, `cheaked_out`, `cheaked_out_time`, `rate_type_id`, `custom_rate`, `deposit_amt`, `source`, `membership`, `remark`, `passport_ic`, `fname`, `lname`, `dob`, `nationality`, `language`, `gender`, `mobile`, `address`, `trace`, `status`, `date_created`, `date_modified`) VALUES
(1, '19200020105', '204', '1562998920', '14:22', '1563006120', '16:22', '1562998920', '14:22', '1562999160', '1562999160', 3, '25', '', '', '', '', 'S918181K', 'Sean Koo', '', '', 'Singapaorean', '', 'Male', '', '', '', 1, '2019-07-13 14:22:50', '2019-07-13 14:26:27'),
(2, '19200021706', '217', '1563011760', '17:56', '1563022560', '20:56', '1563011760', '17:56', '1563030180', '1563030180', 6, '65', '', '', '', '', 'S9101011L', 'Johnny Huang', '', '', 'Sporean', '', 'Male', '', '', '', 1, '2019-07-13 17:57:33', '2019-07-13 23:03:02'),
(3, '19200020107', '205', '1563030420', '23:07', '1563451200', '00:07', '1563030420', '23:07', '1563451200', '1563451200', 1, '15', '25', '', '', 'Jane Hu\r\nS91010101U', 'S81717161Y', 'Bernard Too', '', '1959-07-07', 'SG', '', 'Male', '', '', '', 1, '2019-07-13 23:07:51', '2019-07-18 20:11:18'),
(4, '19200020108', '201', '1563099480', '18:18', '1563106680', '20:18', '1563099480', '18:18', '1563106680', '1563106680', 3, '25', '40', '', '', '', 'G1234567Y', 'Gotham Tamal', '', '1954-07-05', 'Indian', '', 'Male', '', '', '', 1, '2019-07-14 18:19:53', '2019-07-17 21:53:54'),
(5, '19200040809', '304', '1563099660', '18:21', '1563696000', '16:00', '1563099660', '18:21', '', '', 6, '65', '', '', '', 'Serene \r\nS9101011O', 'S1234567P', 'Fulli Poon', '', '', 'SG', '', 'Male', '', '', 'Check-Out Due', 1, '2019-07-14 18:21:48', '2019-07-21 15:08:14'),
(6, '192000202010', '202', '1563106500', '20:15', '1563117300', '23:15', '1563106500', '20:15', '1563106800', '1563106800', 5, '35', '', '', '', 'testing', 'S1234567Y', 'Testing  Loon', '', '2006-07-06', 'SG', '', 'Male', '', '', '', 1, '2019-07-14 20:20:13', '2019-07-14 20:20:29'),
(7, '192000203011', '207', '1563107340', '20:29', '1563110940', '21:29', '1563107340', '20:29', '1563107460', '1563107460', 1, '15', '', '', '', '', 'SS123U', 'John Yeo', '', '2019-07-02', 'SG', '', 'Male', '', '', '', 1, '2019-07-14 20:30:06', '2019-07-14 20:31:04'),
(8, '192000309012', '309', '1563452220', '20:17', '1563546540', '22:29', '1563452220', '20:17', '1563692880', '1563692880', 3, '25', '40', '', '', 'HELLO', 'S55818192O', 'Muhhamed Ali', 'Franklin', '1991-09-28', 'Timorese', '', 'Male', '', '', '', 1, '2019-07-18 20:18:39', '2019-07-21 15:10:00'),
(9, '192000401013', '301', '1563692460', '15:01', '1563698700', '14:15', '1563692460', '15:01', '', '', 1, '15', '40', '', '', 'Hello', 'S1234567I', 'Tamal', 'Hardi', '1958-07-02', 'SG', '', 'Male', '', '', 'Check-Out Due', 1, '2019-07-21 15:02:51', '2019-08-01 15:36:45'),
(10, '192000306014', '317', '1564653300', '15:25', '1564727700', '12:05', '1564653300', '15:25', '', '', 6, '65', '', '', '', 'ok', 'S8181817P', 'Muhhamed', 'Ali', '1989-08-02', 'SG', '', 'Male', '', '', 'Check-Out Due', 1, '2019-08-01 22:37:40', '2019-08-01 22:38:08'),
(11, '192000307015', '307', '1564743480', '16:28', '1564747080', '17:28', '1564743480', '16:28', '1564912560', '1564912560', 1, '15', '', '', '', 'OK\r\n', 'S12345J', 'JO', 'LI', '2019-08-02', 'SG', '', 'Male', '', '', '', 1, '2019-08-02 15:08:30', '2019-08-04 17:56:21'),
(12, '192000414016', '414', '1564733460', '13:41', '1564737060', '14:41', '1564733460', '13:41', '', '', 1, '15', '', '', '', '', 'iaua91', 'Tamak', 'Loo', '2019-08-01', 'Indian', '', 'Female', '', '', 'Check-Out Due', 1, '2019-08-02 15:11:59', '2019-08-02 15:11:59'),
(13, '192000611017', '611', '1564738200', '15:00', '1565334900', '15:15', '1564738200', '15:00', '', '', 6, '65', '', '', '', 'Test', '123456', 'Gaurav', 'Singh', '25-09-1995', 'Indian', '', 'Male', '', '', 'Check-Out Due', 1, '2019-08-02 15:22:42', '2019-08-03 21:55:31'),
(14, '192000203014', '419', '1566274380', '11:22', '1566360000', '12:00', '1566271320', '11:22', '', '', 0, '888', '888', '', '', 'sdfsdfdf', 'IDKJH24567', 'Gaurav', 'Singh', '01-08-2019', 'GER', '', 'Male', '', '', 'Check-Out Due', 1, '2019-08-20 13:53:31', '2019-08-27 18:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `branch_master`
--

CREATE TABLE `branch_master` (
  `id` int(100) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1' COMMENT '0-Incactive,1-Active',
  `date_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch_master`
--

INSERT INTO `branch_master` (`id`, `branch_name`, `status`, `date_time`) VALUES
(1, 'Lucknow ', 1, '2019-06-03 18:29:08'),
(2, 'Ghaziabad', 1, '2019-06-03 18:33:08');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(220) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `image` varchar(220) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `email`, `phone`, `mobile`, `address`, `gst_no`, `pan_no`, `image`, `date_created`, `date_modify`, `status`) VALUES
(1, 'Kavyashri Foodworks', 'kavyashrifoodworks@gmail.com', '01204120400', '9873002074', '3rd floor shipra mall indrapuram ghaziabad-201014', '09AATFK0734E1Z8', 'BPCPM8404E', '1/1logo1.png', '2018-12-15 16:12:53', '2019-06-03 17:50:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `confic`
--

CREATE TABLE `confic` (
  `id` int(11) NOT NULL,
  `confic_type_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `control` varchar(200) DEFAULT 'text'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `confic`
--

INSERT INTO `confic` (`id`, `confic_type_id`, `title`, `value`, `description`, `control`) VALUES
(1, 1, 'paging', '100', 'Page should be #5, 10, 20, 50, 100, 1000', 'select'),
(3, 1, 'default_mail', 'narai1987@gmail.com', 'Default mail to communicate with user', 'text'),
(4, 1, 'max_count', '100', '', ''),
(5, 2, 'language', '1', '', ''),
(6, 1, 'service_tax', '8', '', ''),
(10, 1, 'join_point', '100', '', ''),
(11, 1, 'rating_point', '10', '', ''),
(12, 1, 'get_point_on_purchase_product_per_1000_USD', '100', '', ''),
(13, 1, 'login_point', '5', '', ''),
(14, 1, 'price_per_100_point', '2', '', ''),
(15, 1, 'admin_gift_point', '50', '', ''),
(16, 1, 'minimum_used_point', '500', '', ''),
(17, 1, 'point_per_beverage', '5', '', ''),
(18, 1, 'point_per_eqipment', '5', '', ''),
(19, 1, 'point_per_food', '5', '', ''),
(20, 1, 'point_per_cabin', '10', '', ''),
(21, 1, 'trip_low_price_range', '5000', '', 'text'),
(22, 1, 'trip_high_price_range', '25000', '', 'text');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `num_code` int(3) NOT NULL DEFAULT '0',
  `alpha_2_code` varchar(2) DEFAULT NULL,
  `alpha_3_code` varchar(3) DEFAULT NULL,
  `en_short_name` varchar(52) DEFAULT NULL,
  `nationality` varchar(39) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`num_code`, `alpha_2_code`, `alpha_3_code`, `en_short_name`, `nationality`) VALUES
(4, 'AF', 'AFG', 'Afghanistan', 'Afghan'),
(8, 'AL', 'ALB', 'Albania', 'Albanian'),
(10, 'AQ', 'ATA', 'Antarctica', 'Antarctic'),
(12, 'DZ', 'DZA', 'Algeria', 'Algerian'),
(16, 'AS', 'ASM', 'American Samoa', 'American Samoan'),
(20, 'AD', 'AND', 'Andorra', 'Andorran'),
(24, 'AO', 'AGO', 'Angola', 'Angolan'),
(28, 'AG', 'ATG', 'Antigua and Barbuda', 'Antiguan or Barbudan'),
(31, 'AZ', 'AZE', 'Azerbaijan', 'Azerbaijani, Azeri'),
(32, 'AR', 'ARG', 'Argentina', 'Argentine'),
(36, 'AU', 'AUS', 'Australia', 'Australian'),
(40, 'AT', 'AUT', 'Austria', 'Austrian'),
(44, 'BS', 'BHS', 'Bahamas', 'Bahamian'),
(48, 'BH', 'BHR', 'Bahrain', 'Bahraini'),
(50, 'BD', 'BGD', 'Bangladesh', 'Bangladeshi'),
(51, 'AM', 'ARM', 'Armenia', 'Armenian'),
(52, 'BB', 'BRB', 'Barbados', 'Barbadian'),
(56, 'BE', 'BEL', 'Belgium', 'Belgian'),
(60, 'BM', 'BMU', 'Bermuda', 'Bermudian, Bermudan'),
(64, 'BT', 'BTN', 'Bhutan', 'Bhutanese'),
(68, 'BO', 'BOL', 'Bolivia (Plurinational State of)', 'Bolivian'),
(70, 'BA', 'BIH', 'Bosnia and Herzegovina', 'Bosnian or Herzegovinian'),
(72, 'BW', 'BWA', 'Botswana', 'Motswana, Botswanan'),
(74, 'BV', 'BVT', 'Bouvet Island', 'Bouvet Island'),
(76, 'BR', 'BRA', 'Brazil', 'Brazilian'),
(84, 'BZ', 'BLZ', 'Belize', 'Belizean'),
(86, 'IO', 'IOT', 'British Indian Ocean Territory', 'BIOT'),
(90, 'SB', 'SLB', 'Solomon Islands', 'Solomon Island'),
(92, 'VG', 'VGB', 'Virgin Islands (British)', 'British Virgin Island'),
(96, 'BN', 'BRN', 'Brunei Darussalam', 'Bruneian'),
(100, 'BG', 'BGR', 'Bulgaria', 'Bulgarian'),
(104, 'MM', 'MMR', 'Myanmar', 'Burmese'),
(108, 'BI', 'BDI', 'Burundi', 'Burundian'),
(112, 'BY', 'BLR', 'Belarus', 'Belarusian'),
(116, 'KH', 'KHM', 'Cambodia', 'Cambodian'),
(120, 'CM', 'CMR', 'Cameroon', 'Cameroonian'),
(124, 'CA', 'CAN', 'Canada', 'Canadian'),
(132, 'CV', 'CPV', 'Cabo Verde', 'Cabo Verdean'),
(136, 'KY', 'CYM', 'Cayman Islands', 'Caymanian'),
(140, 'CF', 'CAF', 'Central African Republic', 'Central African'),
(144, 'LK', 'LKA', 'Sri Lanka', 'Sri Lankan'),
(148, 'TD', 'TCD', 'Chad', 'Chadian'),
(152, 'CL', 'CHL', 'Chile', 'Chilean'),
(156, 'CN', 'CHN', 'China', 'Chinese'),
(158, 'TW', 'TWN', 'Taiwan, Province of China', 'Chinese, Taiwanese'),
(162, 'CX', 'CXR', 'Christmas Island', 'Christmas Island'),
(166, 'CC', 'CCK', 'Cocos (Keeling) Islands', 'Cocos Island'),
(170, 'CO', 'COL', 'Colombia', 'Colombian'),
(174, 'KM', 'COM', 'Comoros', 'Comoran, Comorian'),
(175, 'YT', 'MYT', 'Mayotte', 'Mahoran'),
(178, 'CG', 'COG', 'Congo (Republic of the)', 'Congolese'),
(180, 'CD', 'COD', 'Congo (Democratic Republic of the)', 'Congolese'),
(184, 'CK', 'COK', 'Cook Islands', 'Cook Island'),
(188, 'CR', 'CRI', 'Costa Rica', 'Costa Rican'),
(191, 'HR', 'HRV', 'Croatia', 'Croatian'),
(192, 'CU', 'CUB', 'Cuba', 'Cuban'),
(196, 'CY', 'CYP', 'Cyprus', 'Cypriot'),
(203, 'CZ', 'CZE', 'Czech Republic', 'Czech'),
(204, 'BJ', 'BEN', 'Benin', 'Beninese, Beninois'),
(208, 'DK', 'DNK', 'Denmark', 'Danish'),
(212, 'DM', 'DMA', 'Dominica', 'Dominican'),
(214, 'DO', 'DOM', 'Dominican Republic', 'Dominican'),
(218, 'EC', 'ECU', 'Ecuador', 'Ecuadorian'),
(222, 'SV', 'SLV', 'El Salvador', 'Salvadoran'),
(226, 'GQ', 'GNQ', 'Equatorial Guinea', 'Equatorial Guinean, Equatoguinean'),
(231, 'ET', 'ETH', 'Ethiopia', 'Ethiopian'),
(232, 'ER', 'ERI', 'Eritrea', 'Eritrean'),
(233, 'EE', 'EST', 'Estonia', 'Estonian'),
(234, 'FO', 'FRO', 'Faroe Islands', 'Faroese'),
(238, 'FK', 'FLK', 'Falkland Islands (Malvinas)', 'Falkland Island'),
(239, 'GS', 'SGS', 'South Georgia and the South Sandwich Islands', 'South Georgia or South Sandwich Islands'),
(242, 'FJ', 'FJI', 'Fiji', 'Fijian'),
(246, 'FI', 'FIN', 'Finland', 'Finnish'),
(248, 'AX', 'ALA', 'Åland Islands', 'Åland Island'),
(250, 'FR', 'FRA', 'France', 'French'),
(254, 'GF', 'GUF', 'French Guiana', 'French Guianese'),
(258, 'PF', 'PYF', 'French Polynesia', 'French Polynesian'),
(260, 'TF', 'ATF', 'French Southern Territories', 'French Southern Territories'),
(262, 'DJ', 'DJI', 'Djibouti', 'Djiboutian'),
(266, 'GA', 'GAB', 'Gabon', 'Gabonese'),
(268, 'GE', 'GEO', 'Georgia', 'Georgian'),
(270, 'GM', 'GMB', 'Gambia', 'Gambian'),
(275, 'PS', 'PSE', 'Palestine, State of', 'Palestinian'),
(276, 'DE', 'DEU', 'Germany', 'German'),
(288, 'GH', 'GHA', 'Ghana', 'Ghanaian'),
(292, 'GI', 'GIB', 'Gibraltar', 'Gibraltar'),
(296, 'KI', 'KIR', 'Kiribati', 'I-Kiribati'),
(300, 'GR', 'GRC', 'Greece', 'Greek, Hellenic'),
(304, 'GL', 'GRL', 'Greenland', 'Greenlandic'),
(308, 'GD', 'GRD', 'Grenada', 'Grenadian'),
(312, 'GP', 'GLP', 'Guadeloupe', 'Guadeloupe'),
(316, 'GU', 'GUM', 'Guam', 'Guamanian, Guambat'),
(320, 'GT', 'GTM', 'Guatemala', 'Guatemalan'),
(324, 'GN', 'GIN', 'Guinea', 'Guinean'),
(328, 'GY', 'GUY', 'Guyana', 'Guyanese'),
(332, 'HT', 'HTI', 'Haiti', 'Haitian'),
(334, 'HM', 'HMD', 'Heard Island and McDonald Islands', 'Heard Island or McDonald Islands'),
(336, 'VA', 'VAT', 'Vatican City State', 'Vatican'),
(340, 'HN', 'HND', 'Honduras', 'Honduran'),
(344, 'HK', 'HKG', 'Hong Kong', 'Hong Kong, Hong Kongese'),
(348, 'HU', 'HUN', 'Hungary', 'Hungarian, Magyar'),
(352, 'IS', 'ISL', 'Iceland', 'Icelandic'),
(356, 'IN', 'IND', 'India', 'Indian'),
(360, 'ID', 'IDN', 'Indonesia', 'Indonesian'),
(364, 'IR', 'IRN', 'Iran', 'Iranian, Persian'),
(368, 'IQ', 'IRQ', 'Iraq', 'Iraqi'),
(372, 'IE', 'IRL', 'Ireland', 'Irish'),
(376, 'IL', 'ISR', 'Israel', 'Israeli'),
(380, 'IT', 'ITA', 'Italy', 'Italian'),
(384, 'CI', 'CIV', 'Côte d\'Ivoire', 'Ivorian'),
(388, 'JM', 'JAM', 'Jamaica', 'Jamaican'),
(392, 'JP', 'JPN', 'Japan', 'Japanese'),
(398, 'KZ', 'KAZ', 'Kazakhstan', 'Kazakhstani, Kazakh'),
(400, 'JO', 'JOR', 'Jordan', 'Jordanian'),
(404, 'KE', 'KEN', 'Kenya', 'Kenyan'),
(408, 'KP', 'PRK', 'Korea (Democratic People\'s Republic of)', 'North Korean'),
(410, 'KR', 'KOR', 'Korea (Republic of)', 'South Korean'),
(414, 'KW', 'KWT', 'Kuwait', 'Kuwaiti'),
(417, 'KG', 'KGZ', 'Kyrgyzstan', 'Kyrgyzstani, Kyrgyz, Kirgiz, Kirghiz'),
(418, 'LA', 'LAO', 'Lao People\'s Democratic Republic', 'Lao, Laotian'),
(422, 'LB', 'LBN', 'Lebanon', 'Lebanese'),
(426, 'LS', 'LSO', 'Lesotho', 'Basotho'),
(428, 'LV', 'LVA', 'Latvia', 'Latvian'),
(430, 'LR', 'LBR', 'Liberia', 'Liberian'),
(434, 'LY', 'LBY', 'Libya', 'Libyan'),
(438, 'LI', 'LIE', 'Liechtenstein', 'Liechtenstein'),
(440, 'LT', 'LTU', 'Lithuania', 'Lithuanian'),
(442, 'LU', 'LUX', 'Luxembourg', 'Luxembourg, Luxembourgish'),
(446, 'MO', 'MAC', 'Macao', 'Macanese, Chinese'),
(450, 'MG', 'MDG', 'Madagascar', 'Malagasy'),
(454, 'MW', 'MWI', 'Malawi', 'Malawian'),
(458, 'MY', 'MYS', 'Malaysia', 'Malaysian'),
(462, 'MV', 'MDV', 'Maldives', 'Maldivian'),
(466, 'ML', 'MLI', 'Mali', 'Malian, Malinese'),
(470, 'MT', 'MLT', 'Malta', 'Maltese'),
(474, 'MQ', 'MTQ', 'Martinique', 'Martiniquais, Martinican'),
(478, 'MR', 'MRT', 'Mauritania', 'Mauritanian'),
(480, 'MU', 'MUS', 'Mauritius', 'Mauritian'),
(484, 'MX', 'MEX', 'Mexico', 'Mexican'),
(492, 'MC', 'MCO', 'Monaco', 'Monégasque, Monacan'),
(496, 'MN', 'MNG', 'Mongolia', 'Mongolian'),
(498, 'MD', 'MDA', 'Moldova (Republic of)', 'Moldovan'),
(499, 'ME', 'MNE', 'Montenegro', 'Montenegrin'),
(500, 'MS', 'MSR', 'Montserrat', 'Montserratian'),
(504, 'MA', 'MAR', 'Morocco', 'Moroccan'),
(508, 'MZ', 'MOZ', 'Mozambique', 'Mozambican'),
(512, 'OM', 'OMN', 'Oman', 'Omani'),
(516, 'NA', 'NAM', 'Namibia', 'Namibian'),
(520, 'NR', 'NRU', 'Nauru', 'Nauruan'),
(524, 'NP', 'NPL', 'Nepal', 'Nepali, Nepalese'),
(528, 'NL', 'NLD', 'Netherlands', 'Dutch, Netherlandic'),
(531, 'CW', 'CUW', 'Curaçao', 'Curaçaoan'),
(533, 'AW', 'ABW', 'Aruba', 'Aruban'),
(534, 'SX', 'SXM', 'Sint Maarten (Dutch part)', 'Sint Maarten'),
(535, 'BQ', 'BES', 'Bonaire, Sint Eustatius and Saba', 'Bonaire'),
(540, 'NC', 'NCL', 'New Caledonia', 'New Caledonian'),
(548, 'VU', 'VUT', 'Vanuatu', 'Ni-Vanuatu, Vanuatuan'),
(554, 'NZ', 'NZL', 'New Zealand', 'New Zealand, NZ'),
(558, 'NI', 'NIC', 'Nicaragua', 'Nicaraguan'),
(562, 'NE', 'NER', 'Niger', 'Nigerien'),
(566, 'NG', 'NGA', 'Nigeria', 'Nigerian'),
(570, 'NU', 'NIU', 'Niue', 'Niuean'),
(574, 'NF', 'NFK', 'Norfolk Island', 'Norfolk Island'),
(578, 'NO', 'NOR', 'Norway', 'Norwegian'),
(580, 'MP', 'MNP', 'Northern Mariana Islands', 'Northern Marianan'),
(581, 'UM', 'UMI', 'United States Minor Outlying Islands', 'American'),
(583, 'FM', 'FSM', 'Micronesia (Federated States of)', 'Micronesian'),
(584, 'MH', 'MHL', 'Marshall Islands', 'Marshallese'),
(585, 'PW', 'PLW', 'Palau', 'Palauan'),
(586, 'PK', 'PAK', 'Pakistan', 'Pakistani'),
(591, 'PA', 'PAN', 'Panama', 'Panamanian'),
(598, 'PG', 'PNG', 'Papua New Guinea', 'Papua New Guinean, Papuan'),
(600, 'PY', 'PRY', 'Paraguay', 'Paraguayan'),
(604, 'PE', 'PER', 'Peru', 'Peruvian'),
(608, 'PH', 'PHL', 'Philippines', 'Philippine, Filipino'),
(612, 'PN', 'PCN', 'Pitcairn', 'Pitcairn Island'),
(616, 'PL', 'POL', 'Poland', 'Polish'),
(620, 'PT', 'PRT', 'Portugal', 'Portuguese'),
(624, 'GW', 'GNB', 'Guinea-Bissau', 'Bissau-Guinean'),
(626, 'TL', 'TLS', 'Timor-Leste', 'Timorese'),
(630, 'PR', 'PRI', 'Puerto Rico', 'Puerto Rican'),
(634, 'QA', 'QAT', 'Qatar', 'Qatari'),
(638, 'RE', 'REU', 'Réunion', 'Réunionese, Réunionnais'),
(642, 'RO', 'ROU', 'Romania', 'Romanian'),
(643, 'RU', 'RUS', 'Russian Federation', 'Russian'),
(646, 'RW', 'RWA', 'Rwanda', 'Rwandan'),
(652, 'BL', 'BLM', 'Saint Barthélemy', 'Barthélemois'),
(654, 'SH', 'SHN', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Helenian'),
(659, 'KN', 'KNA', 'Saint Kitts and Nevis', 'Kittitian or Nevisian'),
(660, 'AI', 'AIA', 'Anguilla', 'Anguillan'),
(662, 'LC', 'LCA', 'Saint Lucia', 'Saint Lucian'),
(663, 'MF', 'MAF', 'Saint Martin (French part)', 'Saint-Martinoise'),
(666, 'PM', 'SPM', 'Saint Pierre and Miquelon', 'Saint-Pierrais or Miquelonnais'),
(670, 'VC', 'VCT', 'Saint Vincent and the Grenadines', 'Saint Vincentian, Vincentian'),
(674, 'SM', 'SMR', 'San Marino', 'Sammarinese'),
(678, 'ST', 'STP', 'Sao Tome and Principe', 'São Toméan'),
(682, 'SA', 'SAU', 'Saudi Arabia', 'Saudi, Saudi Arabian'),
(686, 'SN', 'SEN', 'Senegal', 'Senegalese'),
(688, 'RS', 'SRB', 'Serbia', 'Serbian'),
(690, 'SC', 'SYC', 'Seychelles', 'Seychellois'),
(694, 'SL', 'SLE', 'Sierra Leone', 'Sierra Leonean'),
(702, 'SG', 'SGP', 'Singapore', 'Singaporean'),
(703, 'SK', 'SVK', 'Slovakia', 'Slovak'),
(704, 'VN', 'VNM', 'Vietnam', 'Vietnamese'),
(705, 'SI', 'SVN', 'Slovenia', 'Slovenian, Slovene'),
(706, 'SO', 'SOM', 'Somalia', 'Somali, Somalian'),
(710, 'ZA', 'ZAF', 'South Africa', 'South African'),
(716, 'ZW', 'ZWE', 'Zimbabwe', 'Zimbabwean'),
(724, 'ES', 'ESP', 'Spain', 'Spanish'),
(728, 'SS', 'SSD', 'South Sudan', 'South Sudanese'),
(729, 'SD', 'SDN', 'Sudan', 'Sudanese'),
(732, 'EH', 'ESH', 'Western Sahara', 'Sahrawi, Sahrawian, Sahraouian'),
(740, 'SR', 'SUR', 'Suriname', 'Surinamese'),
(744, 'SJ', 'SJM', 'Svalbard and Jan Mayen', 'Svalbard'),
(748, 'SZ', 'SWZ', 'Swaziland', 'Swazi'),
(752, 'SE', 'SWE', 'Sweden', 'Swedish'),
(756, 'CH', 'CHE', 'Switzerland', 'Swiss'),
(760, 'SY', 'SYR', 'Syrian Arab Republic', 'Syrian'),
(762, 'TJ', 'TJK', 'Tajikistan', 'Tajikistani'),
(764, 'TH', 'THA', 'Thailand', 'Thai'),
(768, 'TG', 'TGO', 'Togo', 'Togolese'),
(772, 'TK', 'TKL', 'Tokelau', 'Tokelauan'),
(776, 'TO', 'TON', 'Tonga', 'Tongan'),
(780, 'TT', 'TTO', 'Trinidad and Tobago', 'Trinidadian or Tobagonian'),
(784, 'AE', 'ARE', 'United Arab Emirates', 'Emirati, Emirian, Emiri'),
(788, 'TN', 'TUN', 'Tunisia', 'Tunisian'),
(792, 'TR', 'TUR', 'Turkey', 'Turkish'),
(795, 'TM', 'TKM', 'Turkmenistan', 'Turkmen'),
(796, 'TC', 'TCA', 'Turks and Caicos Islands', 'Turks and Caicos Island'),
(798, 'TV', 'TUV', 'Tuvalu', 'Tuvaluan'),
(800, 'UG', 'UGA', 'Uganda', 'Ugandan'),
(804, 'UA', 'UKR', 'Ukraine', 'Ukrainian'),
(807, 'MK', 'MKD', 'Macedonia (the former Yugoslav Republic of)', 'Macedonian'),
(818, 'EG', 'EGY', 'Egypt', 'Egyptian'),
(826, 'GB', 'GBR', 'United Kingdom of Great Britain and Northern Ireland', 'British, UK'),
(831, 'GG', 'GGY', 'Guernsey', 'Channel Island'),
(832, 'JE', 'JEY', 'Jersey', 'Channel Island'),
(833, 'IM', 'IMN', 'Isle of Man', 'Manx'),
(834, 'TZ', 'TZA', 'Tanzania, United Republic of', 'Tanzanian'),
(840, 'US', 'USA', 'United States of America', 'American'),
(850, 'VI', 'VIR', 'Virgin Islands (U.S.)', 'U.S. Virgin Island'),
(854, 'BF', 'BFA', 'Burkina Faso', 'Burkinabé'),
(858, 'UY', 'URY', 'Uruguay', 'Uruguayan'),
(860, 'UZ', 'UZB', 'Uzbekistan', 'Uzbekistani, Uzbek'),
(862, 'VE', 'VEN', 'Venezuela (Bolivarian Republic of)', 'Venezuelan'),
(876, 'WF', 'WLF', 'Wallis and Futuna', 'Wallis and Futuna, Wallisian or Futunan'),
(882, 'WS', 'WSM', 'Samoa', 'Samoan'),
(887, 'YE', 'YEM', 'Yemen', 'Yemeni'),
(894, 'ZM', 'ZMB', 'Zambia', 'Zambian');

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `id` int(11) NOT NULL,
  `shortcode` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`id`, `shortcode`, `nationality`, `status`) VALUES
(1, 'AMS', 'AMSTERDAM', 1),
(2, 'AUS', 'AUSTRALIA', 1),
(3, 'B/TAN', 'BHTAN', 1),
(4, 'BAN', 'BANGLADESH', 1),
(5, 'BRU', 'BRUNEI', 1),
(6, 'CAM', 'CAMBODIAN', 1),
(7, 'CHINA', 'CHINA', 1),
(8, 'COL', 'COLOMBIA', 1),
(9, 'FRA', 'FRANCE', 1),
(10, 'GER', 'GERMANY', 1),
(11, 'INDIA', 'INDIA', 1),
(12, 'INDO`S', 'INDONESIA', 1),
(13, 'IRAN', 'IRAN', 1),
(14, 'ITA', 'ITALY', 1),
(15, 'JAP', 'JAPAN', 1),
(16, 'KOR', 'KOREA', 1),
(17, 'LEBA', 'LEBANON', 1),
(18, 'MAL', 'MALAYSIA', 1),
(19, 'MAU', 'MAURITIUS', 1),
(20, 'MYAN', 'MYANMAR', 1),
(21, 'OTHER', 'OTHER', 1),
(22, 'PAK', 'PAKISTAN', 1),
(23, 'PHIL', 'PHILIPPINES', 1),
(24, 'SAU', 'SAUDI ARABIA', 1),
(25, 'SING', 'SINGAPORE', 1),
(26, 'SL', 'SRI LANKA', 1),
(27, 'SYR', 'SYRIA', 1),
(28, 'THA', 'THAILAND', 1),
(29, 'UK', 'UNITED KINGDOM', 1),
(30, 'USA', 'UNITED STATES', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rate_master`
--

CREATE TABLE `rate_master` (
  `id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `room_size_id` int(11) NOT NULL,
  `stay_time` varchar(200) NOT NULL,
  `type` varchar(100) NOT NULL,
  `rate` varchar(200) NOT NULL,
  `rate_code` varchar(200) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date_created` varchar(200) NOT NULL,
  `date_modify` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate_master`
--

INSERT INTO `rate_master` (`id`, `room_type_id`, `room_size_id`, `stay_time`, `type`, `rate`, `rate_code`, `remark`, `status`, `date_created`, `date_modify`) VALUES
(0, 1, 1, '', '', '', 'MANUAL', '', 0, '2019-08-20 13:18:41', ''),
(1, 1, 1, '1', 'Hour', '15', 'SS1H', '', 1, '2019-06-04 13:08:05', '2019-07-08 11:53:05'),
(2, 1, 2, '1', 'Hour', '20', 'SD1H', '', 1, '2019-06-04 13:08:05', '2019-07-08 11:55:17'),
(3, 1, 2, '2', 'Hour', '25', 'SD2H', 'Testing', 1, '2019-06-06 13:30:50', '2019-07-08 11:55:28'),
(4, 1, 2, '1', 'Day', '100', 'SD1D', '1 Day Room Booking', 1, '2019-07-09 15:16:27', ''),
(5, 1, 1, '10', 'Hour', '500', 'TEST', '', 1, '2019-08-31 19:10:52', '');

-- --------------------------------------------------------

--
-- Table structure for table `room_master`
--

CREATE TABLE `room_master` (
  `id` bigint(200) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `room_type_id` int(100) NOT NULL,
  `room_size_id` int(100) NOT NULL,
  `capacity` int(100) NOT NULL,
  `room_no` int(100) NOT NULL,
  `booking_id` bigint(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date_created` varchar(100) NOT NULL,
  `date_modify` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_master`
--

INSERT INTO `room_master` (`id`, `branch_id`, `room_type_id`, `room_size_id`, `capacity`, `room_no`, `booking_id`, `status`, `date_created`, `date_modify`) VALUES
(1, 1, 1, 1, 2, 201, 0, 1, '2019-06-04 13:47:37', '2019-07-10 20:38:27'),
(2, 1, 1, 2, 3, 202, 0, 1, '2019-06-04 13:47:37', '2019-07-27 15:46:22'),
(3, 1, 1, 1, 2, 203, 0, 1, '2019-06-04 13:47:37', '2019-08-20 14:32:57'),
(4, 1, 1, 1, 2, 204, 0, 1, '2019-06-04 13:47:37', '2019-07-16 15:36:33'),
(5, 1, 1, 1, 2, 205, 0, 1, '2019-06-04 13:47:37', '2019-07-09 19:40:21'),
(6, 1, 1, 1, 2, 206, 0, 1, '2019-06-04 13:47:37', '2019-06-04 13:47:37'),
(7, 1, 1, 1, 2, 207, 0, 1, '2019-06-04 13:47:37', '2019-06-06 15:42:06'),
(8, 1, 1, 1, 2, 208, 0, 1, '2019-06-04 13:47:37', '2019-06-04 13:47:37'),
(9, 1, 1, 1, 2, 209, 0, 1, '2019-06-04 13:47:37', '2019-06-07 15:38:18'),
(10, 1, 1, 1, 2, 210, 0, 1, '2019-06-04 13:47:37', '2019-06-04 13:47:37'),
(11, 1, 1, 1, 2, 211, 0, 1, '2019-07-06 17:27:33', '2019-07-19 18:08:25'),
(12, 1, 1, 1, 2, 217, 0, 1, '2019-07-06 17:29:00', '2019-07-06 17:29:00'),
(13, 1, 1, 1, 2, 218, 0, 1, '2019-07-06 17:30:36', '2019-07-06 17:30:36'),
(14, 1, 1, 1, 2, 219, 0, 1, '2019-07-06 17:31:04', '2019-07-06 17:31:04'),
(15, 1, 1, 1, 2, 301, 0, 1, '2019-07-06 17:32:03', '2019-07-09 19:40:36'),
(16, 1, 1, 1, 0, 302, 0, 1, '2019-07-12 14:38:49', '2019-07-12 14:38:49'),
(17, 1, 1, 1, 0, 303, 0, 1, '2019-07-12 14:38:58', '2019-07-12 14:38:58'),
(18, 1, 1, 1, 0, 304, 0, 1, '2019-07-12 14:39:05', '2019-07-12 14:39:05'),
(19, 1, 1, 1, 0, 305, 0, 1, '2019-07-12 14:39:12', '2019-07-12 14:39:12'),
(20, 1, 1, 1, 0, 307, 0, 1, '2019-07-12 14:39:20', '2019-07-12 14:39:20'),
(21, 1, 1, 1, 0, 309, 0, 1, '2019-07-12 14:39:30', '2019-07-12 14:39:30'),
(22, 1, 1, 1, 0, 310, 0, 1, '2019-07-12 14:39:37', '2019-07-12 14:39:37'),
(23, 1, 1, 1, 0, 311, 0, 1, '2019-07-12 14:40:00', '2019-07-12 14:40:00'),
(24, 1, 1, 1, 0, 312, 0, 1, '2019-07-12 14:40:08', '2019-07-12 14:40:08'),
(25, 1, 1, 1, 0, 313, 0, 1, '2019-07-12 14:40:14', '2019-07-12 14:40:14'),
(26, 1, 1, 1, 0, 314, 0, 1, '2019-07-12 14:40:21', '2019-07-12 14:40:21'),
(27, 1, 1, 1, 0, 315, 0, 1, '2019-07-12 14:40:29', '2019-07-12 14:40:29'),
(28, 1, 1, 1, 0, 316, 0, 1, '2019-07-12 14:40:35', '2019-07-12 14:40:35'),
(29, 1, 1, 1, 0, 318, 0, 1, '2019-07-12 14:40:46', '2019-07-12 14:40:46'),
(30, 1, 1, 1, 0, 317, 0, 1, '2019-07-12 14:40:57', '2019-07-12 14:40:57'),
(31, 1, 1, 1, 0, 319, 0, 1, '2019-07-12 14:41:05', '2019-07-12 14:41:05'),
(32, 1, 1, 1, 0, 401, 0, 1, '2019-07-12 14:41:26', '2019-07-12 14:41:26'),
(33, 1, 1, 1, 0, 402, 0, 1, '2019-07-12 14:41:48', '2019-07-12 14:41:48'),
(34, 1, 1, 1, 0, 403, 0, 1, '2019-07-12 14:41:59', '2019-07-12 14:41:59'),
(35, 1, 1, 1, 0, 404, 0, 1, '2019-07-12 14:42:07', '2019-07-12 14:42:07'),
(36, 1, 1, 1, 0, 405, 0, 1, '2019-07-12 14:42:13', '2019-07-12 14:42:13'),
(37, 1, 1, 1, 0, 406, 0, 1, '2019-07-12 14:42:20', '2019-07-12 14:42:20'),
(38, 1, 1, 1, 0, 407, 0, 1, '2019-07-12 14:42:28', '2019-07-12 14:42:28'),
(39, 1, 1, 1, 0, 408, 0, 1, '2019-07-12 14:42:36', '2019-07-12 14:42:36'),
(40, 1, 1, 1, 0, 409, 0, 1, '2019-07-12 14:42:49', '2019-07-12 14:42:49'),
(41, 1, 1, 1, 0, 410, 0, 1, '2019-07-12 14:42:55', '2019-07-12 14:42:55'),
(42, 1, 1, 1, 0, 411, 0, 1, '2019-07-12 14:43:09', '2019-07-12 14:43:09'),
(43, 1, 1, 1, 0, 412, 0, 1, '2019-07-12 14:43:15', '2019-07-12 14:43:15'),
(44, 1, 1, 1, 0, 413, 0, 1, '2019-07-12 14:43:23', '2019-07-12 14:43:23'),
(45, 1, 1, 1, 0, 414, 0, 1, '2019-07-12 14:43:29', '2019-07-12 14:43:29'),
(46, 1, 1, 1, 0, 415, 0, 1, '2019-07-12 14:43:35', '2019-07-12 14:43:35'),
(47, 1, 1, 1, 0, 416, 0, 1, '2019-07-12 14:43:42', '2019-07-12 14:43:42'),
(48, 1, 1, 1, 0, 417, 0, 1, '2019-07-12 14:43:49', '2019-07-12 14:43:49'),
(49, 1, 1, 1, 0, 418, 0, 1, '2019-07-12 14:43:55', '2019-07-19 18:08:15'),
(50, 1, 1, 1, 0, 419, 14, 2, '2019-07-12 14:44:03', '2019-07-12 14:44:03'),
(51, 1, 1, 1, 0, 501, 0, 1, '2019-07-12 14:44:50', '2019-07-12 14:44:50'),
(52, 1, 1, 1, 0, 502, 0, 1, '2019-07-12 14:44:57', '2019-07-12 14:44:57'),
(53, 1, 1, 1, 0, 503, 0, 1, '2019-07-12 14:45:16', '2019-07-12 14:45:16'),
(54, 1, 1, 1, 0, 504, 0, 1, '2019-07-12 14:45:22', '2019-07-12 14:45:22'),
(55, 1, 1, 1, 0, 505, 0, 1, '2019-07-12 14:45:29', '2019-07-12 14:45:29'),
(56, 1, 1, 1, 0, 506, 0, 1, '2019-07-12 14:45:41', '2019-07-12 14:45:41'),
(57, 1, 1, 1, 0, 507, 0, 1, '2019-07-12 14:45:48', '2019-07-12 14:45:48'),
(58, 1, 1, 1, 0, 508, 0, 1, '2019-07-12 14:45:54', '2019-07-12 14:45:54'),
(59, 1, 1, 1, 0, 509, 0, 1, '2019-07-12 14:59:31', '2019-07-12 14:59:31'),
(60, 1, 1, 1, 0, 510, 0, 1, '2019-07-12 14:59:38', '2019-07-12 14:59:38'),
(61, 1, 1, 1, 0, 511, 0, 1, '2019-07-12 14:59:45', '2019-07-12 14:59:45'),
(62, 1, 1, 1, 0, 512, 0, 1, '2019-07-12 14:59:51', '2019-07-12 14:59:51'),
(63, 1, 1, 1, 0, 513, 0, 1, '2019-07-12 14:59:58', '2019-07-12 14:59:58'),
(64, 1, 1, 1, 0, 514, 0, 1, '2019-07-12 15:00:05', '2019-07-12 15:00:05'),
(65, 1, 1, 1, 0, 515, 0, 1, '2019-07-12 15:00:13', '2019-07-12 15:00:13'),
(66, 1, 1, 1, 0, 516, 0, 1, '2019-07-12 15:00:19', '2019-07-12 15:00:19'),
(67, 1, 1, 1, 0, 517, 0, 1, '2019-07-12 15:00:44', '2019-07-12 15:00:44'),
(68, 1, 1, 1, 0, 518, 0, 1, '2019-07-12 15:00:50', '2019-07-12 15:00:50'),
(69, 1, 1, 1, 0, 519, 0, 1, '2019-07-12 15:01:03', '2019-07-12 15:01:03'),
(70, 1, 1, 2, 0, 601, 0, 1, '2019-07-12 15:02:24', '2019-07-12 15:02:24'),
(71, 1, 1, 2, 0, 602, 0, 1, '2019-07-12 15:02:31', '2019-07-12 15:02:31'),
(72, 1, 1, 2, 0, 603, 0, 1, '2019-07-12 15:02:42', '2019-07-12 15:02:42'),
(73, 1, 1, 2, 0, 604, 0, 1, '2019-07-12 15:02:51', '2019-07-12 15:02:51'),
(74, 1, 1, 2, 0, 605, 0, 1, '2019-07-12 15:02:58', '2019-07-12 15:02:58'),
(75, 1, 1, 1, 0, 607, 0, 1, '2019-07-12 15:03:13', '2019-07-12 15:03:13'),
(76, 1, 1, 1, 0, 608, 0, 1, '2019-07-12 15:03:20', '2019-07-12 15:03:20'),
(77, 1, 1, 1, 0, 609, 0, 1, '2019-07-12 15:03:29', '2019-07-12 15:03:29'),
(78, 1, 1, 1, 0, 610, 0, 1, '2019-07-12 15:03:36', '2019-07-12 15:03:36'),
(79, 1, 1, 1, 0, 611, 0, 1, '2019-07-12 15:03:42', '2019-07-12 15:03:42'),
(80, 1, 1, 1, 0, 612, 0, 1, '2019-07-12 15:03:50', '2019-07-12 15:03:50'),
(81, 1, 1, 1, 0, 613, 0, 1, '2019-07-12 15:03:56', '2019-07-12 15:03:56'),
(82, 1, 1, 1, 0, 614, 0, 1, '2019-07-12 15:04:03', '2019-07-12 15:04:03'),
(83, 1, 1, 1, 0, 615, 0, 1, '2019-07-12 15:04:10', '2019-07-12 15:04:10'),
(84, 1, 1, 1, 0, 616, 0, 1, '2019-07-12 15:04:15', '2019-07-12 15:04:15'),
(85, 1, 1, 1, 0, 617, 0, 1, '2019-07-12 15:04:21', '2019-07-12 15:04:21'),
(86, 1, 1, 1, 0, 618, 0, 1, '2019-07-12 15:04:28', '2019-07-12 15:04:28'),
(87, 1, 1, 1, 0, 619, 0, 1, '2019-07-12 15:04:36', '2019-07-12 15:04:36'),
(88, 1, 1, 2, 0, 701, 0, 1, '2019-07-12 15:05:03', '2019-07-12 15:05:03'),
(89, 1, 1, 2, 0, 702, 0, 1, '2019-07-12 15:05:11', '2019-07-12 15:05:11'),
(90, 1, 1, 2, 0, 703, 0, 1, '2019-07-12 15:05:17', '2019-07-12 15:05:17'),
(91, 1, 1, 2, 0, 704, 0, 1, '2019-07-12 15:05:24', '2019-07-12 15:05:24'),
(92, 1, 1, 2, 0, 705, 0, 1, '2019-07-12 15:05:36', '2019-07-12 15:05:36'),
(93, 1, 1, 1, 0, 707, 0, 1, '2019-07-12 15:05:52', '2019-07-12 15:05:52'),
(94, 1, 1, 1, 0, 708, 0, 1, '2019-07-12 15:05:58', '2019-07-12 15:05:58'),
(95, 1, 1, 1, 0, 709, 0, 1, '2019-07-12 15:06:09', '2019-07-12 15:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `room_size`
--

CREATE TABLE `room_size` (
  `id` int(100) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1' COMMENT '0-Incactive,1-Active',
  `date_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_size`
--

INSERT INTO `room_size` (`id`, `size`, `status`, `date_time`) VALUES
(1, 'Single', 1, '2019-06-04 13:08:05'),
(2, 'Double', 1, '2019-06-04 13:08:05'),
(3, 'Sweet', 1, '2019-06-06 15:38:43');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `id` int(100) NOT NULL,
  `room_type` varchar(255) NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1' COMMENT '0-Incactive,1-Active',
  `date_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `room_type`, `status`, `date_time`) VALUES
(1, 'Standard', 1, '2019-06-04 13:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tmp_type` int(11) DEFAULT NULL,
  `default_temp` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `name`, `tmp_type`, `default_temp`, `status`) VALUES
(1, 'jet_tmp', 1, 0, 1),
(2, 'api', 0, 0, 1),
(7, 'mw_web', 1, 1, 1),
(8, 'mw', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `scheme_id` varchar(255) DEFAULT NULL COMMENT 'department Wise',
  `date` varchar(255) DEFAULT NULL,
  `utype` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `mobile`, `email`, `scheme_id`, `date`, `utype`, `status`) VALUES
(1, 'admin', 'admin', 'Min Wah Hotel', '9999999999', 'admin@gmail.com', '', '01/02/2017', 'Administrator', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch_master`
--
ALTER TABLE `branch_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `confic`
--
ALTER TABLE `confic`
  ADD PRIMARY KEY (`id`,`confic_type_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`num_code`),
  ADD UNIQUE KEY `alpha_2_code` (`alpha_2_code`),
  ADD UNIQUE KEY `alpha_3_code` (`alpha_3_code`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rate_master`
--
ALTER TABLE `rate_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_master`
--
ALTER TABLE `room_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_size`
--
ALTER TABLE `room_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `branch_master`
--
ALTER TABLE `branch_master`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `confic`
--
ALTER TABLE `confic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `rate_master`
--
ALTER TABLE `rate_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_master`
--
ALTER TABLE `room_master`
  MODIFY `id` bigint(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `room_size`
--
ALTER TABLE `room_size`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
