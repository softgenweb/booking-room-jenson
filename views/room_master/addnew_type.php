<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Type</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=room_master&task=show_type"><i class="fa fa-list" aria-hidden="true"></i> Type List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Type</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Type</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Room Type</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                        <input type="text" value="<?php echo $result['room_type']; ?>" id="room_type" name="room_type" class="form-control"  required="">         
                  </div>
               </div>
               <div class="clearfix"></div>
              
               
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="room_master"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="save_type"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<script type="text/javascript">
   function validation()
   
   {   
      var chk=1;
      
         if(document.getElementById('country_name').value == '') { 
            document.getElementById('msgcountry_name').innerHTML = "*Required field.";
            chk=0;
         }
         else if(!isletter(document.getElementById('country_name').value)) { 
            document.getElementById('msgcountry_name').innerHTML = "*Enter Valid Country Name.";
            chk=0;
         }
         
         else {
            document.getElementById('msgcountry_name').innerHTML = "";
         }
   
            if(chk)
             {       
               return true;      
            }     
            else 
            {     
               return false;     
            }  
         
         
         }
   
</script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
   });
   
     
   function goBack() {
      window.history.back();
   }
</script>

