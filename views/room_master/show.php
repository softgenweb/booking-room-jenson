<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Room Detail</h3>
            <?php foreach($results as $result) { }  ?>
             <a href="index.php?control=room_master&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Room</a> 
             <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total Room : <?php echo $no_of_row; ?>
            </p> 
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Room Detail</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div></th>
                           <!-- <th><div align="center">Branch</div></th>                            -->
                           <th><div align="center">Room Number</div></th>                           
                           <th><div align="center">Type</div></th>                           
                           <th><div align="center">Size</div></th>                           
                           <th><div align="center">Capacity</div></th> 
                           <th><div align="center">Booking Status</div></th>                          
                           <th><div align="center">Action</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>
                           <!-- <td align="center"><strong><?php echo $result['branch_id'];?></strong></td>             -->
                           <td align="center"><strong><?php echo $result['room_no'];?></strong></td>            
                           <td align="center"><?php echo $this->room_type($result['room_type_id']);?></td>            
                           <td align="center"><?php echo $this->room_size($result['room_size_id']);?></td>            
                           <td align="center"><?php echo $result['capacity']." -Person Max";?></td>            
                           <td align="center">
									<?php
                                    if($result['booking_id']!='0' && $result['status']=='2'){  ?>
                                    <b style="color:#0073b7;">Booked</b></a>
                                    <?php } elseif($result['status']=='3') { ?>
                                    <b style="color:#01bb66;">Not-Cleaned</b></a>
                                    <?php } elseif($result['status']=='0') {  ?>  
                                    <b >Not-Available</b></a>
                                    <?php } else {  ?>  
                                    <b style="color:#007741;">Available</b></a>
                                    <?php } ?>
                               </td>
                           <td align="left">
                            <?php   if($result['booking_id']=='0' && $result['status']!='2'){ ?>
                              <a href="index.php?control=room_master&task=addnew&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
                              <?php }
                                 if($result['status']==1){  ?>
                              <a href="index.php?control=room_master&task=status&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                              <?php } elseif($result['status']==0) { ?>
                              <a href="index.php?control=room_master&task=status&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                              <?php } ?>
                            
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

