<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View All Guest Detail</h3>
            <?php foreach($results as $result) { }  ?>
            <!--<a href="index.php?control=branch_master&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Branch</a>--> 
             <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total Guest : <?php echo $no_of_row; ?>
            </p> 
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> All Guest Detail</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
               <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

      <div class="row col-md-12">

        <div class="col-md-12 col-sm-9 col-xs-12">        
                    
                    <div class="col-md-2">
                    <div class="form-group" style="margin-top:5%;">
                    From Date:
                    </div></div> 
                    <div class="col-md-2"><div class="form-group">                    
                   <input type="text" name="from_date" id="from_date" value="<?php echo $_REQUEST['from_date']; ?>" class="form-control" readonly="readonly">
                    <span id="msgcountry_id" style="color:red;"></span>
                    </div></div>
                    
                    <div class="col-md-2">
                    <div class="form-group" style="margin-top:5%;">
                    To Date:
                    </div></div>
                    <div class="col-md-2"><div class="form-group">                    
                      <input type="text" name="to_date" id="to_date" value="<?php echo $_REQUEST['to_date']; ?>" class="form-control" readonly="readonly">
                    <span id="msgstate_id" style="color:red;"></span>
                    </div></div>
                    
                   <!-- <div class="col-md-1">
                    <div class="form-group" style="margin-top:5%;">
                    By:
                    </div></div>
                    <div class="col-md-2"><div class="form-group">
                     <select class="form-control" name="search_by" id="search_by" required>
                     <option value="">Select</option>
                     <option value="date_created" <?php if($_REQUEST['search_by']=='date_created') { ?> selected="selected" <?php } ?>>Booking Date</option>
                     <option value="checked_in" <?php if($_REQUEST['search_by']=='checked_in') { ?> selected="selected" <?php } ?>>Check-in Date</option>
                     <option value="cheaked_out" <?php if($_REQUEST['search_by']=='cheaked_out') { ?> selected="selected" <?php } ?>>Check-out Date</option>
                     </select>                  
                    
                    <span id="msgstate_id" style="color:red;"></span>
                    </div></div>-->
                    
                    <div class="col-md-1">
                    <div class="form-group" style="margin-top:5%;">
                   
                    </div></div>
                    <div class="col-md-2"><div class="form-group">                    
                       <input  class="btn btn-primary butoon_brow" type="submit" name="search" id="btnSelected" value="Search" />
                    <span id="msgstate_id" style="color:red;"></span>
                    </div></div>  

          <input type="hidden" name="control" value="report"/>
          <input type="hidden" name="edit" value="1"/>
          <input type="hidden" name="task" value="show_guest"/>
        </div>
      </div>
    </form>
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div></th>                        
                                                      
                           <th><div align="center">Guest Name</div></th> 
                           <th><div align="center">Passport/IC</div></th> 
                            <th><div align="center">Gender</div></th> 
                            <!-- <th><div align="center">Mobile</div></th>   -->
                            <th><div align="center">Nationality</div></th> 
                             <!-- <th><div align="center">Address</div></th>                         -->
                           <th><div align="center">Check-In</div></th>                           
                           <th><div align="center">Check-Out</div></th>                          
                                                      
                          
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
						   $cin_date = date('d-m-Y H:i',($result['checked_in']));
						     $cout_date = date('d-m-Y H:i',($result['cheaked_out']));
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>
                          
                           <td align="center"><?php echo $result['name'];?></td>
                           <td align="center"><?php echo $result['passport_ic'];?></td>
                           <td align="center"><?php echo $result['gender'];?></td>            
                           <!-- <td align="center"><?php echo $result['mobile'];?></td> -->
                           <!-- <td align="center"><?php echo $result['address'];?></td> -->
                           <td align="center"><?php echo $result['nationality'];?></td>
                           <td align="center"><?php echo $cin_date;?></td>            
                           <td align="center"><?php echo $result['cheaked_out']?$cout_date:"Not Checked-Out Yet";?></td>            
                            
                        </tr>
                         <?php 	
					//$amt += $result['deposit_amt'];
									
						 } ?><?php   }else{?>
                        <?php } ?>
                     </tbody>
                      <!--<tr>                                            
                            <td align="center"></td>                        
                            <td align="center"></td>
                            <td align="center"></td> 
                            <td align="center"></td> 
                            <td align="center"></td> 
                            <td align="center"><b>Total :-</b></td> 
                            <td align="center"><b>$<?php echo $amt; ?></b></td>                       	 
                       
                   </tr>-->
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#from_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y-m-d',
    formatDate:'Y-m-d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
}); 
$('#to_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
     format:'Y-m-d',
    formatDate:'Y-m-d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
}); 
</script>   