<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Sale Amount Detail</h3>
            <?php foreach($results as $result) { }  ?>
            <!--<a href="index.php?control=branch_master&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Branch</a>--> 
             <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total Sale : <?php echo $no_of_row; ?>
            </p> 
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Sale Amount Detail</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                    <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
                        <div class="row col-md-12">
                           <div class="col-md-12">
                              <div class="col-md-2">
                                 <div class="form-group" style="margin-top:5%;">
                                    Check-In From:
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">                    
                                    <input type="text" name="dateFrom" id="from_date" value="<?php echo $_REQUEST['dateFrom']; ?>" class="check_time form-control" required="">
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group" style="margin-top:5%;">
                                    Check-In To:
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">                    
                                    <input type="text" name="dateTo" id="to_date" value="<?php echo $_REQUEST['dateTo']; ?>" class="check_time form-control" required="">
                                 </div>
                              </div>
                              <div class="col-md-1">
                                 <div class="form-group" style="margin-top:5%;">
                                    By:
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">                    
                                    <select class="form-control" name="search_by" id="search_by" required="">
                                      <option value="">Select</option>
                                      <option value="date_created" <?php if($_REQUEST['search_by']=='date_created') { ?> selected="selected" <?php } ?>>Booking Date</option>
                                      <option value="check_in" <?php if($_REQUEST['search_by']=='check_in') { ?> selected="selected" <?php } ?>>Check-in Date</option>
                                      <option value="cheak_out" <?php if($_REQUEST['search_by']=='cheak_out') { ?> selected="selected" <?php } ?>>Check-out Date</option>
                                  </select> 
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                     <!-- </form> -->
                     <hr>
                     <div class="clearfix"></div>
                     <div class="col-md-4 col-md-offset-4">
                        <center>

                              <div class="col-md-1">
                                 <div class="form-group">                    
                                    <input  class="btn btn-primary butoon_brow" type="submit" name="search" id="btnSelected" value="Search" />
                                    <input type="hidden" name="control" value="report"/>
                                    <input type="hidden" name="edit" value="1"/>
                                    <input type="hidden" name="task" value="show"/>
                                 </div>
                              </div>
                              <?php //if($_REQUEST['dateFrom']!=''){ ?>
                              <a href="script/pdf/sale_report.php?dateFrom=<?php echo $_REQUEST['dateFrom']; ?>&dateTo=<?php echo $_REQUEST['dateTo']; ?>&search_by=<?php echo $_REQUEST['search_by']; ?>" class="btn btn-primary">Download</a>
                           <?php //} ?>
                         </center> 
                          </div></form>
                     <div class="clearfix"></div>
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div></th>                        
                           <th><div align="center">Booking Id</div></th>                           
                           <th><div align="center">Room Number</div></th>                           
                           <th><div align="center">Guest Name</div></th>                           
                           <th><div align="center">Check-In</div></th>                           
                           <th><div align="center">Check-Out</div></th>                          
                           <th><div align="center">Booking Amount</div></th>                            
                          
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>
                           <td align="center"><strong> <a href="index.php?control=report&task=booking_detail&booking_id=<?php echo $result['booking_id']; ?>"><?php echo $result['booking_id'];?></a></strong></td>
                           <td align="center"><strong><?php echo $result['room_no'];?></strong></td>
                           <td align="left"><?php echo $result['fname'].' '.$result['lname'];?></td>            
                           <td align="center"><?php echo date("d-m-Y H:i",$result['check_in']);?></td>            
                           <td align="center"><?php echo date("d-m-Y H:i",$result['cheak_out']);?></td>
                           <td align="center"><?php echo "$".$amt = $result['deposit_amt']?$result['deposit_amt']:$result['custom_rate'];?></td> 
                        </tr>
                         <?php 	
					$total += $amt;
									
						 } ?><?php   }else{?>
                        <?php } ?>
                     </tbody>
                      <tr>                                            
                            <td align="center"></td>                        
                            <td align="center"></td>
                            <td align="center"></td> 
                            <td align="center"></td> 
                            <td align="center"></td> 
                            <td align="center"><b>Total :-</b></td> 
                            <td align="center"><b>$<?php echo $total; ?></b></td>                       	 
                       
                   </tr>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->

</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#from_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y-m-d',
    formatDate:'Y-m-d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
}); 
$('#to_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
     format:'Y-m-d',
    formatDate:'Y-m-d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
}); 
</script>   