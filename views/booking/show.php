<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Booking</h3>
            <?php foreach($results as $result) { }  ?>
             <!-- <a href="index.php?control=room_master&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Room</a>  -->
             <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total Booking : <?php echo $no_of_row; ?>
            </p> 
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Booking List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                    <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
                        <div class="row col-md-12">
                           <div class="col-md-12">
                              <div class="col-md-2">
                                 <div class="form-group" style="margin-top:5%;">
                                    Check-In From:
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">                    
                                    <input type="text" name="dateFrom" id="check_in" value="<?php echo $_REQUEST['dateFrom']; ?>" class="check_time form-control" readonly="readonly">
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group" style="margin-top:5%;">
                                    Check-In To:
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">                    
                                    <input type="text" name="dateTo" id="cheak_out" value="<?php echo $_REQUEST['dateTo']; ?>" class="check_time form-control" readonly="readonly">
                                 </div>
                              </div>
                              <div class="col-md-1">
                                 <div class="form-group" style="margin-top:5%;">
                                    Status:
                                 </div>
                              </div>
                              <div class="col-md-2">
                                 <div class="form-group">                    
                                    <select class="form-control" name="status" id="status">
                                       <option value="">Select</option>
                                       <option value="1" <?php echo $_REQUEST['status']=='1'?'selected':''; ?>>Open</option>
                                       <option value="2" <?php echo $_REQUEST['status']=='2'?'selected':''; ?>>Completed</option>
                                    </select>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                     <!-- </form> -->
                     <hr>
                     <div class="clearfix"></div>
                     <div class="col-md-4 col-md-offset-4">
                        <center>

                              <div class="col-md-1">
                                 <div class="form-group">                    
                                    <input  class="btn btn-primary butoon_brow" type="submit" name="search" id="btnSelected" value="Search" />
                                    <input type="hidden" name="control" value="booking"/>
                                    <input type="hidden" name="edit" value="1"/>
                                    <input type="hidden" name="task" value="show"/>
                                 </div>
                              </div>
                              <?php //if($_REQUEST['dateFrom']!=''){ ?>
                              <a href="script/pdf/download_report.php?dateFrom=<?php echo $_REQUEST['dateFrom']; ?>&dateTo=<?php echo $_REQUEST['dateTo']; ?>&status=<?php echo $_REQUEST['status']; ?>" class="btn btn-primary">Download</a>
                           <?php //} ?>
                         </center> 
                          </div></form>
                     <div class="clearfix"></div>

                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div></th>
                           <!-- <th><div align="center">Branch</div></th>                            -->
                           <th><div align="center">Booking Id</div></th>                           
                           <th><div align="center">Room Number</div></th>                           
                           <th><div align="center">Guest Name</div></th>                           
                           <th><div align="center">Passport/IC</div></th>                           
                           <th><div align="center">Check-In</div></th>                           
                           <th><div align="center">Check-Out</div></th>                          
                           <!-- <th><div align="center">Booking Amount</div></th>                             -->
                           <th><div align="center">Status</div></th>                            
                           <th><div align="center">Remark</div></th>                            
                           <th><div align="center">Action</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>
                           <!-- <td align="center"><strong><?php echo $result['branch_id'];?></strong></td>             -->

                           <td align="center"><strong> <a href="index.php?control=booking&task=booking_detail&booking_id=<?php echo $result['booking_id']; ?>"><?php echo $result['booking_id'];?></a></strong></td>
                           <td align="center"><strong><?php echo $result['room_no'];?></strong></td>
                           <td align="center"><?php echo $result['fname']." ".$result['lname'];?></td>            
                           <td align="center"><?php echo $result['passport_ic'];?></td>            
                           <td align="center"><?php echo date("d-m-Y H:i",$result['check_in']);?></td>            
                           <td align="center"><?php echo date("d-m-Y H:i",($result['cheaked_out']?$result['cheaked_out']:$result['cheak_out']));?></td>            
                           <!-- <td align="center"><?php echo "$".$result['deposit_amt'];?></td>             -->
                           <td align="center"><b><?php echo $result['cheaked_out']?"<span>Completed</span>":"<span>Open</span>"; ?></b></td>            
                           <td align="center"><?php  echo $result['remark']?$result['remark']:"N/A"; ?></td>          
                          
                           <td align="center">
                              <a class="btn btn-primary bulu" href="index.php?control=booking&task=booking_detail&booking_id=<?php echo $result['booking_id']; ?>"><b>View</b></a> &nbsp; &nbsp;

                              <!-- <a href="index.php?control=room_master&task=addnew&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp; -->
                              <?php
                               /*  if($result['status']==1){  ?>
                              <a href="index.php?control=room_master&task=status&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                              <?php } else { ?>
                              <a href="index.php?control=room_master&task=status&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                              <?php }*/ ?>
                            
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   $('.arrival').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });    
   $('.check_time').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });   
   
   $(".arrival").on("change",function(){
        var selected = $(this).val();
         $('.arrival').val(selected);
    });
   
   $('.dob').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // maxDate:'+1970/01/01',
      scrollMonth : false
   });
</script>

