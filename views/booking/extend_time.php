<?php foreach($results as $result) { }  ?>
<style type="text/css">
.true-border{
   border: 2px solid #02b311 !important;
}
.false-border{
   border: 2px solid #e60707 !important;
}
   #client_list{float:left;list-style:none;padding:0;width:100%;position: relative;z-index:999999;font-size: 18px;overflow: auto;height:auto; max-height: 240px;border-bottom: 1px solid;position: absolute;width: 18%}
   #client_list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
   #client_list li:hover{background:#6a7a91;cursor: pointer; color:#fff;}
</style>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Extend Stay Time/ Edit Detail</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=booking&task=roomStatus"><i class="fa fa-list" aria-hidden="true"></i> View Rooms</a></li>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Extend Time / Edit Detail</li>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" autocomplete="off" >
          <div class="col-md-10 col-md-offset-1">

         <div class="col-md-6">
   
      <div class="col-md-5">
         <div class="form-group center_text">
           <label>Room No. </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['room_no']; ?>" class=" form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      
          </div>
          <div class="clearfix"></div>
      <div class="col-md-6">
      <h3> Detail</h3>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Check-In </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo date("Y-m-d H:i",$result['check_in']); ?>" placeholder="Check-In Date & Time" id="check_in" name="check_in" class=" arrival form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Rate Type <span><b style="color: red;">*</b></span></label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
           <!--  <input type="text" value="<?php echo $this->rate_type($result['rate_type_id']); ?>"  id="rate_type_id" name="rate_type_id" class=" form-control"  readonly=""> -->

           <select class="form-control select2" onchange="rate_type_change();" name="rate_type_id" id="rate_type_id<?php echo $i; ?>">
              <option value="">Select Rate</option>
              <option value="Manual" <?php echo ($result['rate_type_id']==0)?'selected':''; ?>>Manual</option>
              <?php $rate = mysql_query("SELECT * FROM `rate_master` WHERE `status`=1");
                 while($rate_type = mysql_fetch_array($rate)){
                  ?>
              <option value="<?php echo $rate_type['id']; ?>" <?php echo $result['rate_type_id']==$rate_type['id']?'selected':''; ?>><?php echo $rate_type['rate_code']."- ".$rate_type['stay_time']." ".$rate_type['type']."- $".$rate_type['rate']; ?></option>
              <?php } ?>
           </select>
                             
                   
         </div>
      </div>
      <div class="clearfix"></div>  
      <!-- ========================= -->


      <div id="manual_room_rent" style="display: <?php echo ($result['rate_type_id']==0)?'block':'none'; ?>">
         <div class="col-md-5">
            <div class="form-group center_text">
               <label>Rate</label>
            </div>
         </div>
         <div class="col-md-7">
            <div class="form-group">
               <input type="text" value="<?php echo $result['custom_rate']; ?>" id="manual_room_rent_val" name="manual_room_rent_val" class="form-control"  >         
            </div>
         </div>
         <div class="clearfix"></div>
      </div>

      <!-- ========================== -->
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Check-Out <span><b style="color: red;">*</b></span></label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" maxlength="16" pattern="((?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))) ((0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]))" value="<?php echo date("Y-m-d H:i",$result['cheak_out']); ?>" placeholder="Check-Out Date & Time" id="cheak_out" name="cheak_out" class="  form-control"  required="">    
            <span>Ex: <mark style="padding: unset;">YYYY-MM-DD HH:MM</mark></span>  
            <div id="date_msg" style="display: none;color: #e60707;">* Date Time not Valid</div>
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Actual Checked-Out </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['cheaked_out']?date("Y-m-d H:i",$result['cheaked_out']):"Not Checked-Out yet"; ?>" id="cheaked_out" name="cheaked_out" class=" arrival form-control"  readonly="">  
            <input type="hidden" name="overnight" class="overnight" id="overnight" value="0"/>       
         </div>
      </div>
     <!--  <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Custom Rate</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['custom_rate']; ?>" id="custom_rate<?php echo $i; ?>" name="custom_rate" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Deposit</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['deposit_amt']; ?>" id="deposit_amt" name="deposit_amt" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Source</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['source']; ?>" id="source" name="source" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Membership</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['membership']; ?>" id="membership" name="membership" class="form-control"  readonly="">         
         </div>
      </div> -->
      </div>  
      <div class="col-md-6">
      <h3>Guest Detail</h3>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Passport/IC</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['passport_ic']; ?>" id="passport_ic" name="passport_ic" class="form-control"  required="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>First Name</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['fname']; ?>" id="fname" name="fname" class="form-control"  required="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Last Name</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['lname']; ?>" id="lname" name="lname" class="form-control">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Date of birth</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['dob']?(date('d-m-Y', strtotime($result['dob']))):'N/A'; ?>" id="dob" name="dob" class="dob form-control"  > 
            <div id="date_msg1" style="display: none;color: #e60707;">* Date Time not Valid</div>        
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Nationality</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['nationality']; ?>" id="nationality" name="nationality" class="form-control"  required="">  
            <span id="suggesstion_nationality"></span> 
          <!--   <select class="form-control select2" name="nationality" id="nationality"  required="">
                         <option value="">Select </option>
                         <?php $nation = mysql_query("SELECT * FROM `countries` WHERE 1");
                         while($nationality = mysql_fetch_array($nation)){
               ?>
               <option value="<?php echo $nationality['alpha_3_code']; ?>" <?php echo $result['nationality']==$nationality['alpha_3_code']?'selected':''; ?>><?php echo $nationality['alpha_3_code'].' - '.$nationality['nationality']; ?></option>
                         <?php } ?>
                       </select>   -->    
         </div>
      </div>
      <!-- <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Language</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['language']; ?>" id="language" name="language" class="form-control"  readonly="">         
         </div>
      </div> -->
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Gender</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group"> 
            <!-- <input type="text" value="<?php echo $result['gender']; ?>" id="gender" name="gender" class="form-control" >       -->
             <select class="form-control" name="gender" id="gender" required="">
                <option value="">Select</option>
                <option value="Male" <?php echo $result['gender']=='Male'?'selected':''; ?>>Male</option>
                <option value="Female" <?php echo $result['gender']=='Female'?'selected':''; ?>>Female</option>
                <!-- <option value="Other">Other</option> -->
              </select>
         </div>
      </div>
      <div class="clearfix"></div>
     <!--  <div class="col-md-5">
         <div class="form-group center_text">
            <label>Contact</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['mobile']; ?>" id="mobile" name="mobile" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Address</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <textarea class="form-control" name="address" id="address" readonly=""><?php echo $result['address']; ?></textarea>        
         </div>
      </div> -->
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Remark</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <textarea class="form-control" name="remark" id="remark" ><?php echo $result['remark']; ?></textarea>      
         </div>
      </div>
      </div>
<div class="clearfix"></div>
   <div class="col-md-12"><center>
   <input type="submit" id="extend-btn" name="submit" class="btn btn-primary bulu" value="Extend">&nbsp;
&nbsp;
<a  class="btn btn-primary" href="index.php?control=booking&task=roomStatus" onclick="return confirm('Want to Cancel?');">Cancel</a>
   </div>
   </center>
   <input type="hidden" name="control" value="booking"/>
   <input type="hidden" name="task" value="extend_time_save"/>
   <input type="hidden" name="new_time" id="new_time" value="<?php echo date("Y-m-d H:i",$result['cheak_out']); ?>"/>
   <input type="hidden" name="room_no" value="<?php echo $result['room_no']; ?>"/>
   <input type="hidden" name="id" value="<?php echo $result['id']; ?>"/>
</div>

   </div> 
   </div>
</form>
</div>
</div><!-- table-responsive -->
</div>

<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   $('#cheak_out').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:true,
      format:'Y-m-d H:i',
      formatDate:'Y-m-d',
      minDate:'+1970/01/01',
      scrollMonth : false,
      step:15
   });
   $('#dob').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      format:'d-m-Y',
      formatDate:'d-m-Y',
      maxDate:'+1970/01/01',
      scrollMonth : false,

   });  
        $("#cheak_out").on("change",function(){
        var selected = $(this).val();
         $('#new_time').val(selected);
    });
   function goBack() {
      window.history.back();
   }

   Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
   }
     function rate_type_change(){
    custom_rate_val = $('#rate_type_id').val();
    if(custom_rate_val==''){
         $('#manual_room_rent').hide();
         $('#manual_room_rent_val').prop('required', false);
         $('#cheak_out').val(''); 
         $('#overnight').val("0");        
      }else   if(custom_rate_val != 'Manual'){
   custom_rate = $('#rate_type_id').find('option:selected').text();
   res = custom_rate.split("-");
   rate = res[2].replace("$", "");

   $('#manual_room_rent').hide();
   $('#manual_room_rent_val').prop('required', false).val('');
   // $('#custom_rate'+id).val(rate);
     console.log(res[1]);
   if (res[1].match(/^.*Hour$/)) {
   console.log(res[1].split(" ")[1]);
   addHr = parseInt(res[1].split(" ")[1]);

   
   $('#check_in').val();
   // var a = parseInt(($('#check_in').val()).split(' ')[1].split(":")[0])+addHr;
 
   
   /*===========================*/
   var my_date = $('#check_in').val(); 
     my_date = my_date.replace(/-/g, "/"); 
     var newdate = new Date(my_date).addHours(addHr);
     var month = newdate.getMonth() + 1;
     var date = newdate.getDate() ;
     var hour = (newdate.getHours() < 10 ? '0' + newdate.getHours() : newdate.getHours())  ;
     var min = (newdate.getMinutes() < 10 ? '0' + newdate.getMinutes() : newdate.getMinutes()) ;
     var datetime = newdate.getFullYear() + "-"
     + (month < 10 ? '0' + month : '' + month)  + "-" 
     + (date < 10 ? '0' + date : '' + date) + " " 
     +  (hour) +":"
     +   min ;
            
   console.log(datetime);
   
   /*===========================*/
   
   $('#cheak_out').val(datetime);
   $('#overnight').val("0");
   // console.log(a);
   
   }else if(res[1].match(/^.*Day$/)){

        addDay = parseInt(res[1].split(" ")[1]);
         // addDay =25;
         var my_date = $('#check_in').val(); 
         my_date = my_date.replace(/-/g, "/"); 
        /* var newdate = new Date(my_date);
        var month = newdate.getMonth() + 1;
        var date = newdate.getDate() + addDay ;
        
        var datetime = newdate.getFullYear() + "-"
        + (month < 10 ? '0' + month : '' + month)  + "-" 
        + (date < 10 ? '0' + date : '' + date) + " " 
        +  "12:00";*/
        /*==============================================================*/
         var newdate = new Date(my_date);
         Date.prototype.addDays = function(days) {
             var date = new Date(this.valueOf());
             date.setDate(date.getDate() + days);
             return date;
         }

         var final_date = (newdate.addDays(addDay));
         var datetime = (final_date.toLocaleString("en-CA").slice(0,10)+" 12:00");
         console.log(datetime);
        /*==============================================================*/
   
   
        $('#cheak_out').val(datetime);
        $('#overnight').val("1");
   } }
      else if(custom_rate_val=='Manual'){
         $('#manual_room_rent').show();
         $('#overnight').val("0");
         $('#manual_room_rent_val').prop('required', true).val('');
   /*===========================*/
   var addHr = 3;
   var my_date = $('#check_in').val(); 
   console.log(my_date);
   my_date = my_date.replace(/-/g, "/"); 
   var newdate = new Date(my_date).addHours(addHr);
     var month = newdate.getMonth() + 1;
     var date = newdate.getDate() ;
     var hour = (newdate.getHours() < 10 ? '0' + newdate.getHours() : newdate.getHours())  ;
     var min = (newdate.getMinutes() < 10 ? '0' + newdate.getMinutes() : newdate.getMinutes()) ;
     var datetime = newdate.getFullYear() + "-"
     + (month < 10 ? '0' + month : '' + month)  + "-" 
     + (date < 10 ? '0' + date : '' + date) + " " 
     +  (hour) +":"
     +   min ;
            
   console.log(datetime);
   
   /*===========================*/
   
   $('#cheak_out').val(datetime);
         console.log('Congrats');
      }
   };
   /*
   function rate_type_change(id){
   
   custom_rate = $('#rate_type_id'+id).find('option:selected').text();
   res = custom_rate.split("-");
   rate = res[2].replace("$", "");
   $('#custom_rate'+id).val(rate);
   };*/
   
   /*function current_date(id){
   var currentdate = new Date(); 
   var month = currentdate.getMonth() + 1;
   var date = currentdate.getDate() ;

   var hour = (currentdate.getHours()) ;
   
    var min = (currentdate.getMinutes() < 10 ? '0' + currentdate.getMinutes() : currentdate.getMinutes()) ;
   var datetime = currentdate.getFullYear() + "-"
            + (month < 10 ? '0' + month : '' + month)  + "-" 
            + (date < 10 ? '0' + date : '' + date) + " " 
            +  (hour < 10 ? '0' + hour : '' + hour) +":"
            +  min ;
   
   // console.log(datetime);
   $('#check_in').val(datetime);
   
   }*/
$('#cheak_out').bind('change keyup', function(){
   var val = $(this).val();
   var text = '((?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))) ((0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]))';
   if(val.match(text)){  
          $(this).attr('style','border:1px solid #02b311 !important');
          $('#date_msg').hide();
   }else{
          $(this).attr('style','border:1px solid #e60707 !important');
          $('#date_msg').show();
   }
});

$('#dob').bind('change keyup', function(){
  var val = $(this).val();
  var backway = val.split("-").reverse().join("-");
   
   var text = '(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))';
   if(backway.match(text)){  
          $(this).attr('style','border:1px solid #02b311 !important');
          $('#date_msg1').hide();
          $('#extend-btn').prop('disabled', false);
   }else{
          $(this).attr('style','border:1px solid #e60707 !important');
          $('#date_msg1').show();
          $('#extend-btn').prop('disabled', true);
   }
});
/*$('#cheak_out').blur(function(){
   var val = $(this).val();
   var text = '((?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))) ((0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]))';
   if(!val.match(text)){  
      $('#date_msg').show();
   }else{
      $('#date_msg').hide();
   } 
   });*/
   $('.select2').select2();
      /*=================Nationality=============*/
   $("#nationality").on(" click keyup",  function(){

      $.ajax({
      type: "POST",
      url: "script/nationality.php",
      data:'num='+$(this).val(),
      beforeSend: function(){
         // $("#nationality").css("background","url(LoaderIcon.gif) right 5px bottom 5px no-repeat rgb(255, 255, 255)");
      },
      success: function(data){
         $("#suggesstion_nationality").show();
         $("#suggesstion_nationality").html(data);
         $("#nationality_"+id).css("background","#FFF");
         }
      });

   });

   function selectNationality(val,id) {
      $("#nationality").val(val);
         $("#suggesstion_nationality").hide();
      }  
/*   $('#nationality').change(function(){
      val = $(this).val();
      $('#select2-nationality-container').text(val);
      // $('.select2 option:contains("'+val+'")').text('TEMPEST');
   });

   $(document).ready(function(){
      val = $('#nationality').val();
      $('#select2-nationality-container').text(val);
   });*/

</script>


