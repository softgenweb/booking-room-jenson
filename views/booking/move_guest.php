<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title"> Move Guest</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=booking&task=roomStatus"><i class="fa fa-list" aria-hidden="true"></i> View Rooms</a></li>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Move Guest Room</li>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" autocomplete="off" >
    
          <div class="col-md-10 col-md-offset-1">
            <div class="col-md-6">
   
      <div class="col-md-5">
         <div class="form-group center_text">
           <label>Room No. </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <select class="form-control select2" name="room_no" id="room_no<?php echo $i; ?>" required="">
              <option value="">Select Room</option>
              <?php $room = mysql_query("SELECT * FROM `room_master` WHERE `status`=1");
                 while($room_type = mysql_fetch_array($room)){
                  ?>
              <option value="<?php echo $room_type['room_no']; ?>" <?php echo $result['room_no']==$room_type['room_no']?'selected':''; ?>><?php echo $room_type['room_no']; ?></option>
              <?php } ?>
           </select>      
         </div>
      </div>
      <div class="clearfix"></div>  
      
          </div>
         
          <div class="col-md-6">
                <div class="col-md-5">
         <div class="form-group center_text">
            <label>Old Room No. <span><b style="color: red;">*</b></label>
         </div>
      </div>
        <div class="col-md-7">
         <div class="form-group">
            
            <input type="text" value="<?php echo $result['room_no']; ?>" class=" form-control"  readonly="">                
         </div>
      </div> 
          </div>
          <div class="clearfix"></div>
      <div class="col-md-6">
      <h3> Detail</h3>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Check-In </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo date("Y-m-d H:i",$result['check_in']); ?>" placeholder="Check-In Date & Time" id="check_in" name="check_in" class=" arrival form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Check-Out </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo date("Y-m-d H:i",$result['cheak_out']); ?>" placeholder="Check-Out Date & Time" id="cheak_out" name="cheak_out" class=" arrival form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Rate Type</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo ($guest['rate_type_id']!=0)?($this->rate_type($guest['rate_type_id'])):'Manual'; ?>"  id="rate_type_id" name="rate_type_id" class=" form-control"  readonly="">

           <!-- <select class="form-control select2" onchange="rate_type_change(<?php echo $i; ?>);" name="rate_type_id" id="rate_type_id<?php echo $i; ?>">
              <option value="">Select Rate</option>
              <?php $rate = mysql_query("SELECT * FROM `rate_master` WHERE `status`=1");
                 while($rate_type = mysql_fetch_array($rate)){
                  ?>
              <option value="<?php echo $rate_type['id']; ?>" <?php echo $result['rate_type_id']==$rate_type['id']?'selected':''; ?>><?php echo $rate_type['rate_code']."- ".$rate_type['stay_time']."-".$rate_type['rate']."$"; ?></option>
              <?php } ?>
           </select> -->
                             
                   
         </div>
      </div>
      <div class="clearfix"></div>  
            <!-- ========================= -->


      <div id="manual_room_rent" style="display: <?php echo ($result['rate_type_id']==0)?'block':'none'; ?>">
         <div class="col-md-5">
            <div class="form-group center_text">
               <label>Rate</label>
            </div>
         </div>
         <div class="col-md-7">
            <div class="form-group">
               <input type="text" value="<?php echo $result['custom_rate']; ?>" id="manual_room_rent_val" name="manual_room_rent_val" class="form-control"  readonly>         
            </div>
         </div>
         <div class="clearfix"></div>
      </div>

      <!-- ========================== -->
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Actual Checked-Out </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['cheaked_out']?date("Y-m-d H:i",$result['cheaked_out']):"Not Checked-Out yet"; ?>" id="cheaked_out" name="cheaked_out" class=" arrival form-control"  readonly="">         
         </div>
      </div>
     <!--  <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Custom Rate</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['custom_rate']; ?>" id="custom_rate<?php echo $i; ?>" name="custom_rate" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Deposit</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['deposit_amt']; ?>" id="deposit_amt" name="deposit_amt" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Source</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['source']; ?>" id="source" name="source" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Membership</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['membership']; ?>" id="membership" name="membership" class="form-control"  readonly="">         
         </div>
      </div> -->
     
      </div>  
      <div class="col-md-6">
      <h3>Guest Detail</h3>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Passport/IC</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['passport_ic']; ?>" id="passport_ic" name="passport_ic" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>First Name</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['fname']; ?>" id="name" name="name" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Last Name</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['lname']; ?>" id="name" name="name" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Date of birth</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
          <input type="text" value="<?php echo $result['dob']?(date('d-m-Y', strtotime($result['dob']))):'N/A'; ?>" id="dob" name="dob" class="dob form-control"  readonly="">               
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Nationality</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['nationality']; ?>" id="nationality" name="nationality" class="form-control"  readonly="">         
         </div>
      </div>
      <!-- <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Language</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['language']; ?>" id="language" name="language" class="form-control"  readonly="">         
         </div>
      </div> -->
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Gender</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group"> 
            <input type="text" value="<?php echo $result['gender']; ?>" id="gender" name="gender" class="form-control"  readonly="">      
         </div>
      </div>
      <div class="clearfix"></div>
     <!--  <div class="col-md-5">
         <div class="form-group center_text">
            <label>Contact</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['mobile']; ?>" id="mobile" name="mobile" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Address</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <textarea class="form-control" name="address" id="address" readonly=""><?php echo $result['address']; ?></textarea>        
         </div>
      </div> -->
       <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Remark</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <textarea class="form-control" name="remark" id="remark" readonly=""><?php echo $result['remark']; ?></textarea>      
         </div>
      </div>
      </div>
<div class="clearfix"></div>
<div class="col-md-12"><center>
<input type="submit" name="submit" class="btn btn-primary bulu" value="Move Now">
&nbsp;
&nbsp;
<a  class="btn btn-primary" href="index.php?control=booking&task=roomStatus" onclick="return confirm('Want to Cancel?');">Cancel</a>
</div>
</center>
<input type="hidden" name="control" value="booking"/>
<input type="hidden" name="task" value="move_guest_save"/>
<!-- <input type="hidden" name="new_time" id="new_time" value="<?php echo date("Y-m-d H:i",$result['cheak_out']); ?>"/> -->
<input type="hidden" name="old_room_no" value="<?php echo $result['room_no']; ?>"/>
<input type="hidden" name="id" value="<?php echo $result['id']; ?>"/>
</div>
   </div> 
   </div>
</form>
</div>
</div><!-- table-responsive -->
</div>

<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
 /*  $('#cheak_out').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:true,
      minuteStep: 30,
      format:'Y-m-d H:i',
      formatDate:'Y-m-d',
    //  minDate:'+1970/01/01',
      scrollMonth : false
   }); */ 
        $("#cheak_out").on("change",function(){
        var selected = $(this).val();
         $('#new_time').val(selected);
    });
   function goBack() {
      window.history.back();
   }
      $('.select2').select2();
</script>

