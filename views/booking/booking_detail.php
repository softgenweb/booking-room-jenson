<?php foreach($uresults as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Booking Detail</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Booking List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> View Booking Deatil</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Room</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <!-- <form name="form" method="post" enctype="multipart/form-data" autocomplete="off" > -->
          <div class="col-md-10 col-md-offset-1">
      <div class="col-md-6">
      <h3> Detail</h3>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Check-In </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo date("d-m-Y H:i",$result['check_in']); ?>" placeholder="Check-In Date & Time" id="check_in" name="check_in" class=" arrival form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Check-Out </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo date("d-m-Y H:i",$result['cheak_out']); ?>" placeholder="Check-Out Date & Time" id="cheak_out" name="cheak_out" class=" arrival form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Actual Checked-Out </label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['cheaked_out']?date("d-m-Y H:i",$result['cheaked_out']):"Not Checked-Out yet"; ?>" id="cheaked_out" name="cheaked_out" class=" arrival form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Rate Type</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $this->rate_type($result['rate_type_id']); ?>"  id="rate_type_id" name="rate_type_id" class=" form-control"  readonly="">
                   
         </div>
      </div>
     <!--  <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Custom Rate</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['custom_rate']; ?>" id="custom_rate<?php echo $i; ?>" name="custom_rate" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Deposit</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['deposit_amt']; ?>" id="deposit_amt" name="deposit_amt" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Source</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['source']; ?>" id="source" name="source" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Membership</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['membership']; ?>" id="membership" name="membership" class="form-control"  readonly="">         
         </div>
      </div> -->
      <div class="clearfix"></div>
    
      </div>  
      <div class="col-md-6">
      <h3>Guest Detail</h3>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Passport/IC</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['passport_ic']; ?>" id="passport_ic" name="passport_ic" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>First Name</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['fname']; ?>" id="name" name="name" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Last Name</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['lname']; ?>" id="name" name="name" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>  
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Date of birth</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
           <input type="text" value="<?php echo $result['dob']?(date('d-m-Y', strtotime($result['dob']))):'N/A'; ?>" id="dob" name="dob" class="dob form-control"  readonly="">              
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Nationality</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['nationality']; ?>" id="nationality" name="nationality" class="form-control"  readonly="">         
         </div>
      </div>
      <!-- <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Language</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['language']; ?>" id="language" name="language" class="form-control"  readonly="">         
         </div>
      </div> -->
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Gender</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group"> 
            <input type="text" value="<?php echo $result['gender']; ?>" id="gender" name="gender" class="form-control"  readonly="">      
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Remark</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <textarea class="form-control" name="remark" id="remark" readonly=""><?php echo $result['remark']; ?></textarea>      
         </div>
      </div>
     <!--  <div class="col-md-5">
         <div class="form-group center_text">
            <label>Contact</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <input type="text" value="<?php echo $result['mobile']; ?>" id="mobile" name="mobile" class="form-control"  readonly="">         
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-5">
         <div class="form-group center_text">
            <label>Address</label>
         </div>
      </div>
      <div class="col-md-7">
         <div class="form-group">
            <textarea class="form-control" name="address" id="address" readonly=""><?php echo $result['address']; ?></textarea>        
         </div>
      </div> -->
      </div>
      <div class="clearfix"></div>
   <div class="col-md-12"><center>
<?php //if($result['cheaked_out']==''){ ?>
<a class="btn btn-primary bulu" data-toggle="modal" data-target="#receipt" >Generate Receipt</a>
<?php /*}else{ ?>
<a target="_blank" class="btn btn-primary bulu" href="script/pdf/receipt.php?id=<?php echo $result['id']; ?>" >View Receipt</a>
<?php }*/ ?>
&nbsp;
&nbsp;
<a  class="btn btn-primary" href="index.php?control=booking&task=show" >Back</a>
   </div>
   </center>
   </div> 
      <!--================ Second Table ================-->
   <div id="receipt" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h4 class="modal-title">Generate Receipt</h4></center>
      </div>
      <div class="modal-body">
       <form name="form" method="post" enctype="multipart/form-data" autocomplete="off" >
 <table class="table table-bordered table-striped" style="font-size: 15px;">
            <thead>
              <tr>
                <th colspan="2" style="width: 50%;"><div  align="center">Details</th></div>
                <th colspan="2" style="width: 50%;"><div  align="center">Guest Detail</th></div>
              </tr>
            </thead>
            <tr>
              <td style="width: 145px;"><b>Check-In :</b></td>
              <td><input style="font-size: 15px !important;" type="text" value="<?php echo date('Y-m-d H:i',$result['check_in']); ?>" id="final_check_in" name="final_check_in" class="form-control"  required=""> </td>
              <td><b>Name :</b></td>
              <td><?php echo $result['fname'].' '.$result['lname']; ?></td>
            </tr>
            <tr>
              <td><b>Check-Out :</b></td>
              <td><input style="font-size: 15px !important;" type="text" value="<?php echo date('Y-m-d H:i',$result['cheak_out']); ?>" id="final_check_out" name="final_check_out" class="form-control"  required=""></td>
              <td><b>Gender :</b></td>
              <td><?php echo $result['gender']; ?></td>
            </tr>
            <tr>
              <td><b>Rate Type :</b></td>
              <td><?php echo $this->rate_type($result['rate_type_id']); ?></td>
              <td><b>Passport/IC :</b></td>
              <td><?php echo $result['passport_ic']; ?></td>
            </tr>
            <tr>
              <td><b>Amount :</b></td>
              <td><input type="text" value="<?php echo $result['deposit_amt']?$result['deposit_amt']:$result['custom_rate']; ?>" id="final_amt" name="final_amt" class="form-control"  required=""></td>
              <td ><b>Nationality :</b></td>
              <td><?php echo $result['nationality']; ?></td>
            </tr>
          </table>
       <!--  <div class="col-md-10 col-md-offset-1">
          <div class="col-md-4">
            <div class="form-group center_text">
              <label>Check-In</label>
            </div>
          </div>
          <div class="col-md-8">
            <div class="form-group">
              <input type="text" value="<?php echo date('Y-m-d H:i',$result['check_in']); ?>" id="final_check_in" name="final_check_in" class="form-control"  required="">         
            </div>
          </div> 
          <div class="clearfix"></div>
          <div class="col-md-4">
            <div class="form-group center_text">
              <label>Check-Out</label>
            </div>
          </div>
          <div class="col-md-8">
            <div class="form-group">
              <input type="text" value="<?php echo date('Y-m-d H:i',$result['cheak_out']); ?>" id="final_check_out" name="final_check_out" class="form-control"  required="">         
            </div>
          </div> 
          <div class="clearfix"></div>
          <div class="col-md-4">
            <div class="form-group center_text">
              <label>Amount ($)</label>
            </div>
          </div>
          <div class="col-md-8">
            <div class="form-group">
              <input type="text" value="<?php echo $result['deposit_amt']?$result['deposit_amt']:$result['custom_rate']; ?>" id="final_amt" name="final_amt" class="form-control"  required="">         
            </div>
          </div> 
       
          <div class="clearfix"></div>
        </div> -->  
        <center>
         <div class="col-md-12">
            <input type="submit" name="submit" class="btn btn-primary" value="Submit">
          </div>
          </center>
          <input type="hidden" name="control" value="booking"/>
          <input type="hidden" name="task" value="generate_receipt"/>
          <input type="hidden" name="room_no" value="<?php echo $result['room_no']; ?>"/>    
          <input type="hidden" name="booking_id" value="<?php echo $result['id']; ?>"/>    
          <input type="hidden" name="bid" value="<?php echo $result['booking_id']; ?>"/>    
       </form> 
       <div class="clearfix"></div>
      </div>
      
    </div>

  </div>
</div>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
   });
   
      $('#final_check_in').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:true,
      minuteStep: 30,
      format:'Y-m-d H:i',
      formatDate:'Y-m-d',
      minDate:'+1970/01/01',
      scrollMonth : false
    });   
    $('#final_check_out').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:true,
      minuteStep: 30,
      format:'Y-m-d H:i',
      formatDate:'Y-m-d',
      minDate:'+1970/01/01',
      scrollMonth : false
    });   
   
   function goBack() {
      window.history.back();
   }
</script>

