<section class="content-header">
          <!--<h1>
            Staff Management
          </h1>-->
          <!--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Staff</li>
          </ol>-->
        </section>
<?php foreach($results as $result) { }  ?>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>



<section class="content">
       <div class="panel panel-default">
       <div class="box-header">
 			<h3 class="box-title">Insurer Profile</h3>
       </div>
                        <!--<div class="panel-heading">
                         <a onclick="goBack()" ><i class="fa fa-chevron-circle-left" style="font-size:20px;" title="Back"></i></a>
                        </div>-->
        <div class="panel-body">
        <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">

            <div class="row">

               <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-2">
                <div class="row">
                               
                  
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Insurance Company</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="frmSearch_client">
                         <input type="text"  value="<?php echo $result['officer_company']; ?>" id="officer_company" name="officer_company" class="form-control" autocomplete="off"  > 
                         <div id="suggesstion_client-box"></div>
                    	<span class="msgValid" id="msgclient_name"></span>
                        </div>
                </div>
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Address</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['insurer_address']; ?>" id="insurer_address" name="insurer_address" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msginsurer_address"></span>
                        </div>
                </div>
                
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Name</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['officer_name']; ?>" id="officer_name" name="officer_name" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msgofficer_name"></span>
                        </div>
                </div>
                
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Contact No.</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                       
                    <input type="text" id="officer_contact" name="officer_contact" class="form-control" value="<?php echo $result['officer_contact']; ?>" />
                    <span class="msgValid" id="msgofficer_contact"></span>
                        </div>
                </div>
                
                <div class="form-group oneline" >
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Email</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                       <input type="text"  value="<?php echo $result['officer_email']; ?>" id="officer_email" name="officer_email" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msgofficer_email"></span>
                        </div>
                </div>
                
                <div class="form-group oneline" id="cu_old_name" >
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Remark</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['officer_position']; ?>" id="officer_position" name="officer_position" class="form-control" autocomplete="off" > 
                    	 <span class="msgValid" id="msgofficer_position"></span>
                        </div>
                </div>
                
                              
               
                
                <div class="form-group oneline">    
        		 <div class="col-md-8 col-sm-8 col-xs-12 pull-right">
                 <br />
            
            <?php  
            
            if($_REQUEST['id'] !='')
            { ?>
            <center><input type="submit" name="update" class="btn btn-primary butoon_brow" value="Update"></center>
            <?php } else { ?> 
            <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="Submit"></center>
            <?php } ?>
            	</div>
               </div>
            	
<input type="hidden" name="control" value="profile"/>
<input type="hidden" name="edit" value="1"/>
<input type="hidden" name="task" value="save_insurer"/>
<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
<input type="hidden" name="student_id" id="student_id" value="<?php echo $_SESSION['student_id']; ?>"  />

               </div>
                    
               </div>
               
                   <!--account details ends here-->
          		</div>
            </form>
             </div>
                        </div>
                     
        </section>




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('name').value == '') { 
			document.getElementById('msgname').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!isletter(document.getElementById('name').value)) { 
			document.getElementById('msgname').innerHTML = "*Enter Valid Name.";
			chk=0;
		}
		else {
			document.getElementById('msgname').innerHTML = "";
		}
		
		if(document.getElementById('mobile').value == '') { 
			document.getElementById('msgmobile').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!numericWithoutPoint(document.getElementById('mobile').value)) { 
			document.getElementById('msgmobile').innerHTML = "*Enter Valid Number.";
			chk=0;
		}
		else {
			document.getElementById('msgmobile').innerHTML = "";
		}
		
		
		if(document.getElementById('address').value == '') { 
			document.getElementById('msgaddress').innerHTML = "*Required field.";
		chk=0;
		}
		
		else {
			document.getElementById('msgaddress').innerHTML = "";
		}


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>
<style>
.frmSearch_client {border: 1px solid #a8d4b1;background-color: #c6f7d0;margin: 2px 0px;padding:40px;border-radius:4px;}
#company_list{float:left;list-style:none;padding:0;width:100%;position: relative;z-index:999999;}
#company_list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#company_list li:hover{background:#6a7a91;cursor: pointer; color:#fff;}
#officer_company{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
</style>

<script>
$(document).ready(function(){	 
	
	$("#officer_company").keyup(function(){
		$.ajax({
		type: "POST",
		url: "views/profile/company.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#officer_company").css("background","url(LoaderIcon.gif) right 5px bottom 5px no-repeat rgb(255, 255, 255)");
		},
		success: function(data){
			$("#suggesstion_client-box").show();
			$("#suggesstion_client-box").html(data);
			$("#officer_company").css("background","#FFF");
		}
		});
	});
})
function selectClient(val,objid) {
$("#officer_company").val(val);
$("#suggesstion_client-box").hide();
full_detail(objid)
}
function full_detail(str)
{// alert(str);
	
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		 
			//var data = $.parseJSON(xmlhttp.responseText);
			document.getElementById("insurer_address").value = xmlhttp.responseText;
			
		 //alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/officer_company.php?id="+str, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}


function goBack() {
    window.history.back();
}
</script>

