<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Company</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=company&task=show"><i class="fa fa-list" aria-hidden="true"></i> Company List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Company</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Company</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php   	unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Company Name</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control"  required="">         
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Email</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="email" value="<?php echo $result['email']; ?>" id="email" name="email" class="form-control"  required="" >                        
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Phone</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['phone']; ?>" id="phone" name="phone" class="form-control"  required="">                         
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Mobile</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['mobile']; ?>" id="mobile" name="mobile" class="form-control"  required="" pattern="[0-9]+" >         
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Address</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <textarea id="address" name="address" class="form-control"  required=""><?php echo $result['address']; ?></textarea>         
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>GST No.</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" placeholder="XXABCDEXXXXFXZX" value="<?php echo $result['gst_no']; ?>" id="gst_no" name="gst_no" class="form-control" pattern="[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[1-9a-zA-Z]{1}" maxlength="15" required="">
                     
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>PAN No.</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['pan_no']; ?>"  pattern="[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}" id="pan_no" name="pan_no" class="form-control" maxlength="10" required="">         
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Image</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="file" value="<?php echo $result['image']; ?>" id="image" name="image" class="image" accept="image/*" required="">         
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="company"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="save"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<script type="text/javascript">
   function validation()
   
   {   
   	var chk=1;
   	
   		if(document.getElementById('country_name').value == '') { 
   			document.getElementById('msgcountry_name').innerHTML = "*Required field.";
   			chk=0;
   		}
   		else if(!isletter(document.getElementById('country_name').value)) { 
   			document.getElementById('msgcountry_name').innerHTML = "*Enter Valid Country Name.";
   			chk=0;
   		}
   		
   		else {
   			document.getElementById('msgcountry_name').innerHTML = "";
   		}
   
   			if(chk)
   			 {			
   				return true;		
   			}		
   			else 
   			{		
   				return false;		
   			}	
   		
   		
   		}
   
</script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
   	$(this).alert('close');
   });
   
     
   function goBack() {
      window.history.back();
   }
</script>

