
<div class="detail_right right">
    <div class="detail_container ">
      <div class="head_cont">
        <h2 class="main_head">
          <table width="99%">
            <tr>
              <td width="85%" class="main_txt"><?php if($results[0]['id']=='') { echo "Add New Customer"; } else {echo "Edit Customer";} ?></td>
              <td><!--<a href="#" class="button"><img src="images/add_new.png" alt="add new" /></a>--></td>
            </tr>
          </table>
        </h2>
      </div>
      <div class="grid_container">
        <div class="main_collaps">
          <form name="formLanguage" action="index.php" method="post" enctype="multipart/form-data" onsubmit="return languageValidate();" >
            <div style="width:100%; height:auto;"> 
              <!--TOP TABING START HERE-->
              <div class='tab-container'>
                
                  <div id="lang0_1" class="pans">
                    <table width="98%" cellpadding="0" cellspacing="2" class="tab_regis">
				
            <tr>
               <td align="right" width="30%" valign="top"><strong>First Name :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="fname" id="fname" value="<?php echo $results[0]['fname']; ?>"/>
                <div id="msgfname"></div> 
                
                </td>                    
                
            
           </tr>
           <tr>
               <td align="right" width="30%" valign="top"><strong>Last Name :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="lname" id="lname" value="<?php echo $results[0]['lname']; ?>"/>
                <div id="msglname"></div>
              </td>  
                   
           </tr>
             <tr>
               <td align="right" width="30%" valign="top"><strong>Username :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="username" id="username" value="<?php echo $results[0]['username']; ?>"/>
                <div id="msgusername"></div>
              </td>         
           </tr>
            <tr>
               <td align="right" width="30%" valign="top"><strong>Email-Id :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="email" id="email" value="<?php echo $results[0]['email']; ?>"/>
                 <div id="msgemail"></div>
              </td>         
           </tr>
           <tr>
               <td align="right" width="30%" valign="top"><strong>Password :</strong></td>
                <td colspan="2">
                <input type="password" class="regis_area" name="password" id="password" value="<?php //echo $results[0]['email']; ?>"/>
                <div id="msgpassword"></div>
              </td>         
           </tr>
            
           <tr>
               <td align="right" valign="top"><strong>Mobile :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="mobile" id="mobile" maxlength="10" value="<?php echo $results[0]['mobile']; ?>"/>
                 <div id="msgmobile"></div>
                </td> 
           </tr>
            <tr>
               <td align="right" valign="top"><strong>DOB :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="dob" id="dob" onclick="return date('dob');" readonly="readonly" value="<?php echo $results[0]['dob']; ?>"/>
                </td> 
           </tr>
            <tr>
               <td align="right" valign="top"><strong>Gender :</strong></td>
                <td colspan="2">
               Male &nbsp;<input type="radio" class="reg_txt" name="gender" id="gender" value="1" checked="checked"/> &nbsp; &nbsp;
               Female &nbsp;<input type="radio" class="reg_txt" name="gender" id="gender" value="0"/>
                </td> 
           </tr>
           <tr>
              
                <td align="right" width="30%"><strong>User Type :</strong></td>
                <td class="tr2 bod_right" align="left">
                <select name="registerUser" id="registerUser" class="reg_txt" style="width:100px;" >
                <option value="">Select Type</option>
                <!--<option value="admin">Admin</option>-->
                <option value="2">Editor</option>
                <!--<option value="accountant">Accountant</option>-->
                
                </select>
                <div id="msgregisterUser"></div>
                </td>
           
           </tr>
           
         <!-- <tr>
               <td align="right" width="30%"><strong>Image :</strong></td>
                <td colspan="2">
                <input type="file" class="regis_area" name="image" id="image" />
              </td>         
           </tr>-->
           
           
           
           
                                    
                  </table>
                  </div>
              
              </div>
              <!--TOP TABING END HERE--> 
            </div>
            <table width="100%" cellpadding="0" cellspacing="0" class="tab_button">
              <tr>
                <td width="70%">&nbsp;</td>
                <td align="center" ><button class="lang_button"><strong>Submit</strong></button>
                  <button class="lang_button_re" type="reset"  onclick="resetLanguageValidate()"><strong>Reset</strong></button></td>
              </tr>
            </table><br />

            <input type="hidden" name="control" value="customer"/>
            <input type="hidden" name="edit" value="1"/>
            <input type="hidden" name="task" value="addnew_customer_save"/>
            <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
            
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
