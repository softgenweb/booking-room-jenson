<?php 
error_reporting(0);

date_default_timezone_set('Asia/Singapore');
define('FPDF_FONTPATH', 'font/');
//require('../fpdf.php');

include('../../configuration.php');
include('../../FPDF/tcpdf_include.php');


$result = mysql_fetch_array(mysql_query("SELECT * FROM `booking` WHERE `id`='".$_REQUEST['id']."'"));
/*--------Start FPDF New---------*/
$receipt_no = $result['booking_id'];
$date = date('d-M-Y H:i');
$guest_name = $result['fname'].' '.$result['lname'];
$room_no = $result['room_no'];
$deposit_amt = $result['deposit_amt']?$result['deposit_amt']:$result['custom_rate'];

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->AddPage();

$pdf->SetFont('helvetica','B',20); 
$pdf->SetTextColor(68,68,68);
$pdf->SetTitle('MIN WAH HOTEL - '.$receipt_no);

$Title = "MIN WAH HOTEL";
$title = <<<EOD
$Title
EOD;
$pdf->Write(0, $title, '', 0, 'C', true, 0, false, false, 0);
 
$pdf->SetFont('helvetica','B',10); 
$pdf->SetTextColor(54,70,93);
$title1 = <<<EOD


EOD;
$pdf->Write(0, $title1, '', 0, 'C', true, 0, false, false, 0);

$title2 = <<<EOD
19, Lorong 22 Geylang 
EOD;
$pdf->Write(0, $title2, '', 0, 'C', true, 0, false, false, 0);

$title3 = <<<EOD
SINGAPORE 398676 
EOD;
$pdf->Write(0, $title3, '', 0, 'C', true, 0, false, false, 0);

$title4 = <<<EOD
Tel: 67452219 
EOD;
$pdf->Write(0, $title4, '', 0, 'C', true, 0, false, false, 0);

$title5 = <<<EOD
Fax: 67568468
EOD;
$pdf->Write(0, $title5, '', 0, 'C', true, 0, false, false, 0);


$pdf->SetFont('helvetica','N',10); 
$pdf->Cell(0,10,'',0,5,1); 
$pdf->SetTextColor(51,51,51);
$StrVNO = '<div style="text-align:Left;border-top:1px solid black;"><br>
<b>Receipt Number</b>: '.$receipt_no.'
</div>';
 
$pdf->writeHTML($StrVNO, true, false, true, false, '');

$Date = '<div style="text-align:Left;">
<b>Date</b>: '.$date.'
</div>';
$pdf->writeHTML($Date, true, false, true, false, '');

$Guest = '<br><div style="text-align:Left;">
<b>Guest Name</b>: '.$guest_name.'
</div>';
$pdf->writeHTML($Guest, true, false, true, false, '');
 
 $Room = '<br><div style="text-align:Left;border-bottom:1px solid black;">
<b>Room No</b>: '.$room_no.'
<br></div>';
$pdf->writeHTML($Room, true, false, true, false, '');

 $StrCN = '<br><section>
	<table>
		<thead>
			<tr>
				<th width="50"><u><b>Item</b></u></th>
				<th width="400"><u><b>Description</b></u></th>
				<th><u><b>Amount ($)</b></u></th>
			</tr>
		</thead>
		<tbody>
			<tr><br>
				<td width="50">Stay</td>
				<td width="400">'.date('d-m-Y H:i', $result['check_in']).'  to '.date('d-m-Y H:i', $result['cheaked_out']?$result['cheaked_out']:$result['cheak_out']).'</td>
				<td><b>$</b>'.$deposit_amt.'</td>
			</tr>
		</tbody>
	</table>
</section>';
$pdf->writeHTML($StrCN, true, false, true, false, '');
/*

$pdf->Cell(10,50,'',0,5,1); 
$pdf->SetFont('helvetica','BU',10); 
$pdf->SetTextColor(54,70,93);
$title6 = <<<EOD

EOD;
$pdf->Write(50, $title6, '', 0, 0, true, 0, false, false, 0);

$pdf->SetFont('helvetica','N',10); 
$pdf->SetTextColor(51,51,51);*/

$pdf->Cell(10,50,'',0,5,1); 

$StrDTVR= <<<EOF
<div style="text-align:center;">
<table>
	<tr>
		<td colspan="5" align="left"><small>All prices are in Singapore Dollars.</small> </td>
		<td border="1">Grand Total:</td>
	</tr>
	<tr>
		<td colspan="5" align="left"><small>All prices are inclusive of GST.</small></td>
		<td border="1"><b>$</b>$deposit_amt/-</td>
	</tr>
</table>
</div>

EOF;
$pdf->writeHTML($StrDTVR, true, false, true, false, '');



// $pdfcontent = $pdf->Output("allotment.pdf", "S");
/*if($_REQUEST['welcome']==='214215412jgfrgds234234dfdfd'){
$pdfcontent = $pdf->Output("allotment.pdf", "D"); }*/

$pdf->Output();
?>