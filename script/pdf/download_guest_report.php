<?php 
error_reporting(0);

date_default_timezone_set('Asia/Singapore');
define('FPDF_FONTPATH', 'font/');
//require('../fpdf.php');

include('../../configuration.php');
include('../../FPDF/tcpdf_include.php');

 $dateFrom  = $_REQUEST['dateFrom'];
 $dateTo  = date('Y-m-d', strtotime($_REQUEST['dateTo'] . ' +1 day'));
/*$status = ($_REQUEST['status']==1)?" AND `cheaked_out`=''":
($_REQUEST['status']==2?" AND `cheaked_out`!=''":"");*/
$print_date = date('d-M-Y');
$dateUse = $_REQUEST['dateFrom']?(" AND `date_created` LIKE '".$_REQUEST['dateFrom']."%'"):(" AND `date_created` LIKE '".$_REQUEST['dateTo']."%'");


$dateFromTo = ($dateFrom && $_REQUEST['dateTo'])?(' AND `date_created` between "'.$dateFrom.'" and "'.$dateTo.'"'):$dateUse;


// $uquery = "SELECT * FROM `booking` WHERE 1 $dateFromTo $status  ORDER BY `id` DESC ";

$date_from = $_REQUEST['dateFrom']?$_REQUEST['dateFrom']:"All";
$date_to = $_REQUEST['dateTo']?$_REQUEST['dateTo']:"All";
// $status_now = ($_REQUEST['status']==1)?"Open":($_REQUEST['status']==2?"Completed":"All");
$date_range = ($_REQUEST['dateFrom'] && $_REQUEST['dateTo'])?('Date From: '.$date_from.' To: '.$date_to):
				(($_REQUEST['dateFrom'] && $_REQUEST['dateTo']=='')?('Date: '.$date_from):(($_REQUEST['dateFrom']=='' && $_REQUEST['dateTo'])?('Date: '.$date_to):'Date: All'));
/*--------Start FPDF New---------*/

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->AddPage();

$pdf->SetFont('helvetica','B',18); 
$pdf->SetTextColor(68,68,68);
$pdf->SetTitle('MIN WAH HOTEL - Report');

$Title = "Min Wah Hotel Guest History";
$title = <<<EOD
$Title
EOD;
$pdf->Write(0, $title, '', 0, 'C', true, 0, false, false, 0);
 
$pdf->SetFont('helvetica','B',10); 
$pdf->SetTextColor(54,70,93);
$title1 = <<<EOD


EOD;
$pdf->Write(0, $title1, '', 0, 'C', true, 0, false, false, 0);

$title2 = <<<EOD
$date_range
EOD;
$pdf->Write(0, $title2, '', 0, 'C', true, 0, false, false, 0);

/*$title3 = <<<EOD
SINGAPORE 398676 
EOD;
$pdf->Write(0, $title3, '', 0, 'C', true, 0, false, false, 0);

$title4 = <<<EOD
Tel: 67452219 
EOD;
$pdf->Write(0, $title4, '', 0, 'C', true, 0, false, false, 0);

$title5 = <<<EOD
Fax: 67568468
EOD;
$pdf->Write(0, $title5, '', 0, 'C', true, 0, false, false, 0);*/


$pdf->SetFont('helvetica','N',10); 
$pdf->Cell(0,2,'',0,0,1);
$pdf->SetTextColor(51,51,51);
/*$StrVNO = '<div style="text-align:Left;border-top:1px solid black;"><br>
<b>Receipt Number</b>: 
</div>';
 
$pdf->writeHTML($StrVNO, true, false, true, false, '');

$Date = '<div style="text-align:Left;">
<b>Date</b>: 
</div>';
$pdf->writeHTML($Date, true, false, true, false, '');

$Guest = '<br><div style="text-align:Left;">
<b>Guest Name</b>: 
</div>';
$pdf->writeHTML($Guest, true, false, true, false, '');
 
 $Room = '<br><div style="text-align:Left;border-bottom:1px solid black;">
<b>Room No</b>: 
<br></div>';
$pdf->writeHTML($Room, true, false, true, false, '');*/
$pdf->Cell(0,0,'',0,5,1); 
$table_body = '<br><section>
	<table border="1">
		<thead>
 			<tr>
               <th width="30"><div align="center"><b>S.No</b><br></div></th>                              
               <th width="120"><div align="center"><b>Guest Name</b></div></th>                           
               <th><div align="center"><b>Passport/IC</b></div></th>                       
               <th><div align="center"><b>Gender</b></div></th>                            
               <th><div align="center"><b>Nationality</b></div></th>                            
               <th><div align="center"><b>Room No.</b></div></th>                            
               <th><div align="center"><b>Check-In</b></div></th>                           
               <th><div align="center"><b>Check-Out</b></div></th>  
            </tr>
		</thead><tbody>';

// echo "SELECT * FROM `booking` WHERE 1 $dateFromTo $status  ORDER BY `id` DESC ";
$get_detail = mysql_query("SELECT * FROM `booking` WHERE 1 $dateFromTo $status  ORDER BY `id` ASC ");

		$i=0;
		while ($result = mysql_fetch_array($get_detail)) {
			$i++;
	
		$table_body = $table_body.'
 			<tr>
               <td width="30"><div align="center"><b>'.$i.'</b></div></td>                         
               <td width="120"><div align="left">&nbsp;&nbsp; '.$result['fname']." ".$result['lname'].'</div></td>                       
               <td><div align="center">'.$result['passport_ic'].'</div></td>                       
               <td><div align="center">'.$result['gender'].'</div></td>                       
               <td><div align="center">'.$result['nationality'].'</div></td>                       
               <td><div align="center">'.$result['room_no'].'</div></td>                       
               <td><div align="center">'.date("d-m-Y H:i",$result['check_in']).'</div></td>                       
               <td><div align="center">'.($result['cheaked_out']?date("d-m-Y H:i",$result['cheaked_out']):'Not Checked-Out Yet').'</div></td>
            </tr>
		';}
		$table_body = $table_body.'</tbody>
	</table>
</section>';
$case_idhtml = <<<EOD
	$table_body

EOD;
$pdf->writeHTML($case_idhtml, true, false, true, false, '');
// $pdf->writeHTML($table_body, true, false, true, false, '');
/*

$pdf->Cell(10,50,'',0,5,1); 
$pdf->SetFont('helvetica','BU',10); 
$pdf->SetTextColor(54,70,93);
$title6 = <<<EOD

EOD;
$pdf->Write(50, $title6, '', 0, 0, true, 0, false, false, 0);

$pdf->SetFont('helvetica','N',10); 
$pdf->SetTextColor(51,51,51);*/

/*$pdf->Cell(10,50,'',0,5,1); 

$StrDTVR= <<<EOF
<div style="text-align:center;">
<table>
	<tr>
		<td colspan="5" align="left"><small>All prices are in Singapore Dollars.</small> </td>
		<td border="1">Grand Total:</td>
	</tr>
	<tr>
		<td colspan="5" align="left"><small>All prices are inclusive of GST.</small></td>
		<td border="1"><b>$</b>$status/-</td>
	</tr>
</table>
</div>

EOF;
$pdf->writeHTML($StrDTVR, true, false, true, false, '');
*/


// $pdfcontent = $pdf->Output("allotment.pdf", "S");
// if($_REQUEST['welcome']==='214215412jgfrgds234234dfdfd'){
$pdfcontent = $pdf->Output("Guest_history_report_".$print_date.".pdf", "D");
 // }

$pdf->Output();
?>