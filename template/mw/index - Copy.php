
 
  <body class="hold-transition skin-blue sidebar-mini">
    
<div class="wrapper">
      <?php include_once('includes/header.php');?>
      
      <!-- Left side column. contains the logo and sidebar -->
       <?php include_once('includes/sidebar.php');?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!--<section class="content-header">
          <h1>
            Dashboard
            <!--<small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>-->

        <!-- Main content -->
        
        <?php include_once('controller.php'); ?>
        
        <!-- /.content -->
      </div><!-- /.content-wrapper -->
     
     
      <!-- Left side column. contains the logo and sidebar -->
       <?php include_once('includes/footer.php');?>
