<?php
   header("Pragma: no-cache");
   header("Cache-Control: no-cache");
   header("Expires: 0");
   
   ?>
<!DOCTYPE html>
<html>
   <head>
      <!--<base href="/solution_guru/admin/" >-->
      <base href="" >
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>MW <?php echo $_REQUEST['task']?'- '.ucwords(str_replace('_', ' ', $_REQUEST['task'])):''; ?></title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo $tmp;?>/style.css">
      <!-- Font Awesome -->
      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
      <link rel="stylesheet" href="<?php echo $tmp;?>/bootstrap/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- DataTables -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/datatables/dataTables.bootstrap.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/dist/css/AdminLTE.min.css">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/dist/css/skins/_all-skins.min.css">
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/datatables/dataTables.bootstrap.css">
      <!-- Theme style -->
      <!-- <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
         AdminLTE Skins. Choose a skin from the css/skins
              folder instead of downloading all of them to reduce the load. 
         <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">-->
      <!-------------------------------Start Popup-------------------------------->
      <link rel="stylesheet" type="text/css" href="assets/popup/css/reveal.css" media="all" />
      <script type="text/javascript" src="assets/popup/js/jquery-1.4.1.min.js"></script>
      <script type="text/javascript" src="assets/popup/js/jquery.reveal.js"></script>
      <!-------------------------------End Popup-------------------------------->
      <script type="text/javascript">
         var jq = $.noConflict();
      </script>
      <script src="assets/javascript/script.js"></script>
      <link href="assets/rating/rating.css" rel="stylesheet" type="text/css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script type="text/javascript" src="assets/rating/rating.js"></script>
      <style type="text/css">
         .overall-rating{font-size: 14px;margin-top: 5px;color: #8e8d8d;}
      </style>

      <script type="text/javascript">
         function paging(str) {
                    //  alert(str);
          try {
          document.getElementById("task").value=document.getElementById("defftask").value;
          }
          catch(e) {  
          }
          document.getElementById("page").value = str;
          document.getElementById("tpages").value = document.getElementById("filter").value;
          document.filterForm.submit();
         }
         
         function paging1(str) { 
          //alert(str);
          document.getElementById("task").value = (document.getElementById("task").value)=="delete"?'':(document.getElementById("task").value);
          document.getElementById("page").value = 1;
          document.getElementById("tpages").value = str;
          //alert(document.getElementById("tpages").value);
          document.filterForm.submit();
         }
      </script>  
      <style>
         .pagination {
         border: 1px solid #ccc;
         border-radius: 4px;
         display: inline-block;
         margin: 10px;
         padding: 10px;
         }
         .page {
         border: 1px solid #ccc;
         border-radius: 4px;
         padding: 3px;
         cursor: pointer;
         }
         .page.active {
         background: #30a5ff none repeat scroll 0 0;
         border: medium none;
         color: #fff !important;
         }
         td, th {
         padding: 5px;
         }
         thead {
         background: #f5f5f5 none repeat scroll 0 0;
         }
         .navbar-default {
         background-color: #fff !important;
         border-color: #e7e7e7;
         }
          .skin-blue.sidebar-mini.sidebar-collapse .sidebar-menu{
            margin-left:-25px;
            width: 50px;
          }
          .skin-blue.sidebar-mini.sidebar-collapse .sidebar-menu li a ,
          .skin-blue.sidebar-mini.sidebar-collapse .sidebar-menu li ul {
            background-color: #1554b7;
          }  
          .skin-blue.sidebar-mini.sidebar-collapse .sidebar-menu li.active > a {
           background-color: #4e7fcb;
          }

 

      </style>
      <!-------------------------------Start Dropdown Search -------------------------------->
      <script src="assets/date_picker/jquery.js"></script> 
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/select2/select2.min.css">
      <script type="text/javascript" src="<?php echo $tmp;?>/plugins/select2/select2.min.js"></script>
      <!-------------------------------End Dropdown Search -------------------------------->
   </head>
    <!-- <body class="hold-transition skin-blue sidebar-mini"> -->
   <body class="skin-blue sidebar-mini sidebar-collapse">
      <div class="wrapper">
         <div class="main-header"> 
          <!-- <a href="#" class="hidden-lg hidden-md hidden-sm sidebar-toggle" data-toggle="offcanvas" role="button"> -->
         <a href="javascript:;" class=" sidebar-toggle" data-toggle="offcanvas" role="button" style="color: #fff !important;">
            <span class="sr-only">Toggle navigation</span>
            </a>
         </div>
         <?php //include_once('includes/header.php');?>
         <!-- Left side column. contains the logo and sidebar -->
         <?php include_once('includes/sidebar.php');?>
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!--  <section class="content-header">
               <h1>
                 Dashboard
                <small>Control panel</small>
               </h1>
               <ol class="breadcrumb">
                 <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                 <li class="active">Dashboard</li>
               </ol>
               </section>-->
            <!-- Main content -->
            <!-- <nav class="navbar navbar-default navbar-fixed">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="index.php" style="padding-top: 0;"><img src="" width="100"></a> 
                  </div>
                  <div class="collapse navbar-collapse">
                     <ul class="nav navbar-nav navbar-left">
                     </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="logout.php">
                              <p>Welcome <?php  echo $_SESSION['admin_name'];?> (Log out)</p>
                           </a>
                        </li>
                        <li class="separator hidden-lg"></li>
                     </ul>
                  </div>
               </div>
            </nav> -->
            <section class="content">
               <?php include_once('controller.php'); ?>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
         <?php include_once('includes/footer.php');?>
         <div class="control-sidebar-bg"></div>
      </div>

   </body>
</html>

