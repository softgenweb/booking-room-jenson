

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_ip` varchar(200) NOT NULL,
  `activity` varchar(220) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

INSERT INTO activity_log VALUES("1","","Add Branch Created by admin","1","2019-06-03 18:33:08","1");
INSERT INTO activity_log VALUES("2","","Add Room Created by admin","1","2019-06-04 13:47:37","1");
INSERT INTO activity_log VALUES("3","","Update Room(101) Created by admin","1","2019-06-04 14:00:14","1");
INSERT INTO activity_log VALUES("4","","Update Room(102) Created by admin","1","2019-06-04 18:08:57","1");
INSERT INTO activity_log VALUES("5","","Book Room(102) Created by admin","1","2019-06-05 17:01:15","1");
INSERT INTO activity_log VALUES("6","","Book Room(105) Created by admin","1","2019-06-05 17:04:00","1");
INSERT INTO activity_log VALUES("7","","Add Rate Created by admin","1","2019-06-06 13:30:50","1");
INSERT INTO activity_log VALUES("8","","Update Rate Created by admin","1","2019-06-06 13:34:18","1");
INSERT INTO activity_log VALUES("9","","Update Room(101) Created by admin","1","2019-06-06 13:36:16","1");
INSERT INTO activity_log VALUES("10","","Update Room(101) Created by admin","1","2019-06-06 13:36:32","1");
INSERT INTO activity_log VALUES("11","","Change Status of Room (1) by admin","1","2019-06-06 13:38:40","1");
INSERT INTO activity_log VALUES("12","","Change Status of Rate (3) by admin","1","2019-06-06 13:39:45","1");
INSERT INTO activity_log VALUES("13","","Change Status of Rate (3) by admin","1","2019-06-06 13:40:11","1");
INSERT INTO activity_log VALUES("14","","Update Type Created by admin","1","2019-06-06 15:24:56","1");
INSERT INTO activity_log VALUES("15","","Update Type Created by admin","1","2019-06-06 15:25:11","1");
INSERT INTO activity_log VALUES("16","","Change Status of Type (1) by admin","1","2019-06-06 15:25:18","1");
INSERT INTO activity_log VALUES("17","","Change Status of Type (1) by admin","1","2019-06-06 15:25:24","1");
INSERT INTO activity_log VALUES("18","","Update Size Created by admin","1","2019-06-06 15:38:13","1");
INSERT INTO activity_log VALUES("19","","Update Size Created by admin","1","2019-06-06 15:38:26","1");
INSERT INTO activity_log VALUES("20","","Add Size Created by admin","1","2019-06-06 15:38:43","1");
INSERT INTO activity_log VALUES("21","","Change Status of Size (3) by admin","1","2019-06-06 15:38:52","1");
INSERT INTO activity_log VALUES("22","","Change Status of Size (3) by admin","1","2019-06-06 15:39:07","1");
INSERT INTO activity_log VALUES("23","","Change Status of Room (1) by admin","1","2019-06-06 15:46:27","1");
INSERT INTO activity_log VALUES("24","","Book Room(109) Created by admin","1","2019-06-06 15:53:22","1");
INSERT INTO activity_log VALUES("25","","Book Room(102) Created by admin","1","2019-06-06 18:20:43","1");
INSERT INTO activity_log VALUES("26","","Book Room(103) Created by admin","1","2019-06-07 11:31:43","1");
INSERT INTO activity_log VALUES("27","","Book Room(103) Created by admin","1","2019-06-07 11:48:16","1");
INSERT INTO activity_log VALUES("28","","Book Room(110) Created by admin","1","2019-06-07 11:49:14","1");
INSERT INTO activity_log VALUES("29","","Book Room(102) Created by admin","1","2019-06-07 11:54:51","1");
INSERT INTO activity_log VALUES("30","","Change Status of Room (7) by admin","1","2019-06-07 15:02:04","1");
INSERT INTO activity_log VALUES("31","","Book Room(103) Created by admin","1","2019-06-07 17:28:51","1");
INSERT INTO activity_log VALUES("32","","Book Room(102) Created by admin","1","2019-06-10 10:21:46","1");
INSERT INTO activity_log VALUES("33","","Book Room(103) Created by admin","1","2019-06-10 10:35:12","1");
INSERT INTO activity_log VALUES("34","","Book Room(101) Created by admin","1","2019-06-10 16:40:09","1");
INSERT INTO activity_log VALUES("35","","Book Room(104) Created by admin","1","2019-06-11 14:48:35","1");
INSERT INTO activity_log VALUES("36","","Book Room(102) Created by admin","1","2019-07-06 18:22:07","1");
INSERT INTO activity_log VALUES("37","","Book Room(105) Created by admin","1","2019-07-06 18:45:26","1");
INSERT INTO activity_log VALUES("38","","Update Rate Created by admin","1","2019-07-08 11:53:05","1");
INSERT INTO activity_log VALUES("39","","Update Rate Created by admin","1","2019-07-08 11:55:17","1");
INSERT INTO activity_log VALUES("40","","Update Rate Created by admin","1","2019-07-08 11:55:28","1");
INSERT INTO activity_log VALUES("41","","Book Room(103) Created by admin","1","2019-07-08 15:20:42","1");
INSERT INTO activity_log VALUES("42","","Book Room(104) Created by admin","1","2019-07-09 14:59:28","1");
INSERT INTO activity_log VALUES("43","","Add Rate Created by admin","1","2019-07-09 15:16:27","1");
INSERT INTO activity_log VALUES("44","","Book Room(102) Created by admin","1","2019-07-09 17:12:42","1");
INSERT INTO activity_log VALUES("45","","Book Room(211) Created by admin","1","2019-07-16 19:09:29","1");
INSERT INTO activity_log VALUES("46","","Book Room(418) Created by admin","1","2019-07-19 10:33:10","1");
INSERT INTO activity_log VALUES("47","","Book Room(202) Created by admin","1","2019-07-19 18:09:35","1");





CREATE TABLE `booking` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(255) NOT NULL,
  `room_no` varchar(255) NOT NULL,
  `check_in` varchar(200) NOT NULL,
  `check_in_time` varchar(200) NOT NULL,
  `cheak_out` varchar(200) NOT NULL,
  `cheak_out_time` varchar(200) NOT NULL,
  `checked_in` varchar(200) NOT NULL,
  `checked_in_time` varchar(200) NOT NULL,
  `cheaked_out` varchar(200) NOT NULL,
  `cheaked_out_time` varchar(200) NOT NULL,
  `rate_type_id` int(100) NOT NULL,
  `custom_rate` varchar(255) NOT NULL,
  `deposit_amt` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `membership` varchar(255) NOT NULL,
  `remark` text NOT NULL,
  `passport_ic` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `trace` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date_created` varchar(255) NOT NULL,
  `date_modified` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO booking VALUES("1","19200010301","110","1562578200","15:00","1562585400","17:00","1562578200","15:00","","","3","25","","","","TEsjkjh","jksdfhj","Gsingh","","2019-07-01","dsfdsf","","Male","","","Check-Out Due","1","2019-07-08 15:20:41","2019-07-09 14:58:15");
INSERT INTO booking VALUES("2","19200010402","204","1562664540","14:59","1563276600","16:59","1562664540","14:59","1563276600","1563276600","3","25","500","","","Test","Gstys","Gsingh Test 11","","2019-07-01","INDian","","Male","","","","1","2019-07-09 14:59:28","2019-07-16 15:36:33");
INSERT INTO booking VALUES("3","19200010203","102","1562672520","17:12","1562740200","12:00","1562672520","17:12","1562678940","1562678940","4","25","","","","fdgfdgdfgfd","dfgdfgdfg","fgdfgdfgdgdfg","dfgdfg","2019-07-01","dfgdfgdfgfd","","","","","","1","2019-07-09 17:12:39","2019-07-09 18:59:42");
INSERT INTO booking VALUES("4","19200021104","211","1563284340","19:09","1563345000","20:09","1563284340","19:09","1563345000","1563345000","2","25","300","","","dgdfgdfgdfg","TEsjsdfhj","Gsingh","Test","2019-07-01","Indian","","Male","","","","1","2019-07-16 19:09:29","2019-07-17 12:42:05");
INSERT INTO booking VALUES("5","19200041805","418","1563512520","10:32","1563604200","12:00","1563512520","10:32","1563604200","1563604200","4","100","120","","","Test","dsfsdf","sdfdfsdfsdfsd","fsdfsdfsdfsdfsdf","2019-07-19","indian","","Male","","","","1","2019-07-19 10:33:10","2019-07-19 15:35:57");
INSERT INTO booking VALUES("6","19200020206","202","1563540900","18:08","1564066500","20:25","1563539880","18:08","","","3","25","51","","","Testing purpose","123456879","Gaurav","Singh","2019-06-30","Indian","","Male","","","","1","2019-07-19 18:09:35","2019-07-19 18:32:40");





CREATE TABLE `branch_master` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(255) NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1' COMMENT '0-Incactive,1-Active',
  `date_time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO branch_master VALUES("1","Lucknow ","1","2019-06-03 18:29:08");
INSERT INTO branch_master VALUES("2","Ghaziabad","1","2019-06-03 18:33:08");





CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `image` varchar(220) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO company VALUES("1","Kavyashri Foodworks","kavyashrifoodworks@gmail.com","01204120400","9873002074","3rd floor shipra mall indrapuram ghaziabad-201014","09AATFK0734E1Z8","BPCPM8404E","1/1logo1.png","2018-12-15 16:12:53","2019-06-03 17:50:08","1");





CREATE TABLE `confic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `confic_type_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `control` varchar(200) DEFAULT 'text',
  PRIMARY KEY (`id`,`confic_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO confic VALUES("1","1","paging","100","Page should be #5, 10, 20, 50, 100, 1000","select");
INSERT INTO confic VALUES("3","1","default_mail","narai1987@gmail.com","Default mail to communicate with user","text");
INSERT INTO confic VALUES("4","1","max_count","100","","");
INSERT INTO confic VALUES("5","2","language","1","","");
INSERT INTO confic VALUES("6","1","service_tax","8","","");
INSERT INTO confic VALUES("10","1","join_point","100","","");
INSERT INTO confic VALUES("11","1","rating_point","10","","");
INSERT INTO confic VALUES("12","1","get_point_on_purchase_product_per_1000_USD","100","","");
INSERT INTO confic VALUES("13","1","login_point","5","","");
INSERT INTO confic VALUES("14","1","price_per_100_point","2","","");
INSERT INTO confic VALUES("15","1","admin_gift_point","50","","");
INSERT INTO confic VALUES("16","1","minimum_used_point","500","","");
INSERT INTO confic VALUES("17","1","point_per_beverage","5","","");
INSERT INTO confic VALUES("18","1","point_per_eqipment","5","","");
INSERT INTO confic VALUES("19","1","point_per_food","5","","");
INSERT INTO confic VALUES("20","1","point_per_cabin","10","","");
INSERT INTO confic VALUES("21","1","trip_low_price_range","5000","","text");
INSERT INTO confic VALUES("22","1","trip_high_price_range","25000","","text");





CREATE TABLE `rate_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_id` int(11) NOT NULL,
  `room_size_id` int(11) NOT NULL,
  `stay_time` varchar(200) NOT NULL,
  `type` varchar(100) NOT NULL,
  `rate` varchar(200) NOT NULL,
  `rate_code` varchar(200) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date_created` varchar(200) NOT NULL,
  `date_modify` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO rate_master VALUES("1","1","1","1","Hour","15","SS1H","","1","2019-06-04 13:08:05","2019-07-08 11:53:05");
INSERT INTO rate_master VALUES("2","1","2","1","Hour","20","SD1H","","1","2019-06-04 13:08:05","2019-07-08 11:55:17");
INSERT INTO rate_master VALUES("3","1","2","2","Hour","25","SD2H","Testing","1","2019-06-06 13:30:50","2019-07-08 11:55:28");
INSERT INTO rate_master VALUES("4","1","2","1","Day","100","SD1D","1 Day Room Booking","1","2019-07-09 15:16:27","");





CREATE TABLE `room_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `room_type_id` int(100) NOT NULL,
  `room_size_id` int(100) NOT NULL,
  `capacity` int(100) NOT NULL,
  `room_no` int(100) NOT NULL,
  `booking_id` bigint(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date_created` varchar(100) NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

INSERT INTO room_master VALUES("1","1","1","1","2","201","0","1","2019-06-04 13:47:37","2019-07-10 20:38:27");
INSERT INTO room_master VALUES("2","1","1","2","3","202","6","2","2019-06-04 13:47:37","2019-07-10 14:50:03");
INSERT INTO room_master VALUES("3","1","1","1","2","203","0","1","2019-06-04 13:47:37","2019-07-09 14:46:59");
INSERT INTO room_master VALUES("4","1","1","1","2","204","0","3","2019-06-04 13:47:37","2019-07-16 15:36:33");
INSERT INTO room_master VALUES("5","1","1","1","2","205","0","1","2019-06-04 13:47:37","2019-07-09 19:40:21");
INSERT INTO room_master VALUES("6","1","1","1","2","206","0","1","2019-06-04 13:47:37","2019-06-04 13:47:37");
INSERT INTO room_master VALUES("7","1","1","1","2","207","0","1","2019-06-04 13:47:37","2019-06-06 15:42:06");
INSERT INTO room_master VALUES("8","1","1","1","2","208","0","1","2019-06-04 13:47:37","2019-06-04 13:47:37");
INSERT INTO room_master VALUES("9","1","1","1","2","209","0","1","2019-06-04 13:47:37","2019-06-07 15:38:18");
INSERT INTO room_master VALUES("10","1","1","1","2","210","0","1","2019-06-04 13:47:37","2019-06-04 13:47:37");
INSERT INTO room_master VALUES("11","1","1","1","2","211","0","1","2019-07-06 17:27:33","2019-07-19 18:08:25");
INSERT INTO room_master VALUES("12","1","1","1","2","217","0","1","2019-07-06 17:29:00","2019-07-06 17:29:00");
INSERT INTO room_master VALUES("13","1","1","1","2","218","0","1","2019-07-06 17:30:36","2019-07-06 17:30:36");
INSERT INTO room_master VALUES("14","1","1","1","2","219","0","1","2019-07-06 17:31:04","2019-07-06 17:31:04");
INSERT INTO room_master VALUES("15","1","1","1","2","301","0","1","2019-07-06 17:32:03","2019-07-09 19:40:36");
INSERT INTO room_master VALUES("20","1","1","1","0","302","0","1","2019-07-12 14:38:49","2019-07-12 14:38:49");
INSERT INTO room_master VALUES("21","1","1","1","0","303","0","1","2019-07-12 14:38:58","2019-07-12 14:38:58");
INSERT INTO room_master VALUES("22","1","1","1","0","304","0","1","2019-07-12 14:39:05","2019-07-12 14:39:05");
INSERT INTO room_master VALUES("23","1","1","1","0","305","0","1","2019-07-12 14:39:12","2019-07-12 14:39:12");
INSERT INTO room_master VALUES("24","1","1","1","0","307","0","1","2019-07-12 14:39:20","2019-07-12 14:39:20");
INSERT INTO room_master VALUES("25","1","1","1","0","309","0","1","2019-07-12 14:39:30","2019-07-12 14:39:30");
INSERT INTO room_master VALUES("26","1","1","1","0","310","0","1","2019-07-12 14:39:37","2019-07-12 14:39:37");
INSERT INTO room_master VALUES("27","1","1","1","0","311","0","1","2019-07-12 14:40:00","2019-07-12 14:40:00");
INSERT INTO room_master VALUES("28","1","1","1","0","312","0","1","2019-07-12 14:40:08","2019-07-12 14:40:08");
INSERT INTO room_master VALUES("29","1","1","1","0","313","0","1","2019-07-12 14:40:14","2019-07-12 14:40:14");
INSERT INTO room_master VALUES("30","1","1","1","0","314","0","1","2019-07-12 14:40:21","2019-07-12 14:40:21");
INSERT INTO room_master VALUES("31","1","1","1","0","315","0","1","2019-07-12 14:40:29","2019-07-12 14:40:29");
INSERT INTO room_master VALUES("32","1","1","1","0","316","0","1","2019-07-12 14:40:35","2019-07-12 14:40:35");
INSERT INTO room_master VALUES("33","1","1","1","0","318","0","1","2019-07-12 14:40:46","2019-07-12 14:40:46");
INSERT INTO room_master VALUES("34","1","1","1","0","317","0","1","2019-07-12 14:40:57","2019-07-12 14:40:57");
INSERT INTO room_master VALUES("35","1","1","1","0","319","0","1","2019-07-12 14:41:05","2019-07-12 14:41:05");
INSERT INTO room_master VALUES("36","1","1","1","0","401","0","1","2019-07-12 14:41:26","2019-07-12 14:41:26");
INSERT INTO room_master VALUES("37","1","1","1","0","402","0","1","2019-07-12 14:41:48","2019-07-12 14:41:48");
INSERT INTO room_master VALUES("38","1","1","1","0","403","0","1","2019-07-12 14:41:59","2019-07-12 14:41:59");
INSERT INTO room_master VALUES("39","1","1","1","0","404","0","1","2019-07-12 14:42:07","2019-07-12 14:42:07");
INSERT INTO room_master VALUES("40","1","1","1","0","405","0","1","2019-07-12 14:42:13","2019-07-12 14:42:13");
INSERT INTO room_master VALUES("41","1","1","1","0","406","0","1","2019-07-12 14:42:20","2019-07-12 14:42:20");
INSERT INTO room_master VALUES("42","1","1","1","0","407","0","1","2019-07-12 14:42:28","2019-07-12 14:42:28");
INSERT INTO room_master VALUES("43","1","1","1","0","408","0","1","2019-07-12 14:42:36","2019-07-12 14:42:36");
INSERT INTO room_master VALUES("44","1","1","1","0","409","0","1","2019-07-12 14:42:49","2019-07-12 14:42:49");
INSERT INTO room_master VALUES("45","1","1","1","0","410","0","1","2019-07-12 14:42:55","2019-07-12 14:42:55");
INSERT INTO room_master VALUES("46","1","1","1","0","411","0","1","2019-07-12 14:43:09","2019-07-12 14:43:09");
INSERT INTO room_master VALUES("47","1","1","1","0","412","0","1","2019-07-12 14:43:15","2019-07-12 14:43:15");
INSERT INTO room_master VALUES("48","1","1","1","0","413","0","1","2019-07-12 14:43:23","2019-07-12 14:43:23");
INSERT INTO room_master VALUES("49","1","1","1","0","414","0","1","2019-07-12 14:43:29","2019-07-12 14:43:29");
INSERT INTO room_master VALUES("50","1","1","1","0","415","0","1","2019-07-12 14:43:35","2019-07-12 14:43:35");
INSERT INTO room_master VALUES("51","1","1","1","0","416","0","1","2019-07-12 14:43:42","2019-07-12 14:43:42");
INSERT INTO room_master VALUES("52","1","1","1","0","417","0","1","2019-07-12 14:43:49","2019-07-12 14:43:49");
INSERT INTO room_master VALUES("53","1","1","1","0","418","0","1","2019-07-12 14:43:55","2019-07-19 18:08:15");
INSERT INTO room_master VALUES("54","1","1","1","0","419","0","1","2019-07-12 14:44:03","2019-07-12 14:44:03");
INSERT INTO room_master VALUES("55","1","1","1","0","501","0","1","2019-07-12 14:44:50","2019-07-12 14:44:50");
INSERT INTO room_master VALUES("56","1","1","1","0","502","0","1","2019-07-12 14:44:57","2019-07-12 14:44:57");
INSERT INTO room_master VALUES("57","1","1","1","0","503","0","1","2019-07-12 14:45:16","2019-07-12 14:45:16");
INSERT INTO room_master VALUES("58","1","1","1","0","504","0","1","2019-07-12 14:45:22","2019-07-12 14:45:22");
INSERT INTO room_master VALUES("59","1","1","1","0","505","0","1","2019-07-12 14:45:29","2019-07-12 14:45:29");
INSERT INTO room_master VALUES("60","1","1","1","0","506","0","1","2019-07-12 14:45:41","2019-07-12 14:45:41");
INSERT INTO room_master VALUES("61","1","1","1","0","507","0","1","2019-07-12 14:45:48","2019-07-12 14:45:48");
INSERT INTO room_master VALUES("62","1","1","1","0","508","0","1","2019-07-12 14:45:54","2019-07-12 14:45:54");
INSERT INTO room_master VALUES("63","1","1","1","0","509","0","1","2019-07-12 14:59:31","2019-07-12 14:59:31");
INSERT INTO room_master VALUES("64","1","1","1","0","510","0","1","2019-07-12 14:59:38","2019-07-12 14:59:38");
INSERT INTO room_master VALUES("65","1","1","1","0","511","0","1","2019-07-12 14:59:45","2019-07-12 14:59:45");
INSERT INTO room_master VALUES("66","1","1","1","0","512","0","1","2019-07-12 14:59:51","2019-07-12 14:59:51");
INSERT INTO room_master VALUES("67","1","1","1","0","513","0","1","2019-07-12 14:59:58","2019-07-12 14:59:58");
INSERT INTO room_master VALUES("68","1","1","1","0","514","0","1","2019-07-12 15:00:05","2019-07-12 15:00:05");
INSERT INTO room_master VALUES("69","1","1","1","0","515","0","1","2019-07-12 15:00:13","2019-07-12 15:00:13");
INSERT INTO room_master VALUES("70","1","1","1","0","516","0","1","2019-07-12 15:00:19","2019-07-12 15:00:19");
INSERT INTO room_master VALUES("71","1","1","1","0","517","0","1","2019-07-12 15:00:44","2019-07-12 15:00:44");
INSERT INTO room_master VALUES("72","1","1","1","0","518","0","1","2019-07-12 15:00:50","2019-07-12 15:00:50");
INSERT INTO room_master VALUES("73","1","1","1","0","519","0","1","2019-07-12 15:01:03","2019-07-12 15:01:03");
INSERT INTO room_master VALUES("74","1","1","2","0","601","0","1","2019-07-12 15:02:24","2019-07-12 15:02:24");
INSERT INTO room_master VALUES("75","1","1","2","0","602","0","1","2019-07-12 15:02:31","2019-07-12 15:02:31");
INSERT INTO room_master VALUES("76","1","1","2","0","603","0","1","2019-07-12 15:02:42","2019-07-12 15:02:42");
INSERT INTO room_master VALUES("77","1","1","2","0","604","0","1","2019-07-12 15:02:51","2019-07-12 15:02:51");
INSERT INTO room_master VALUES("78","1","1","2","0","605","0","1","2019-07-12 15:02:58","2019-07-12 15:02:58");
INSERT INTO room_master VALUES("79","1","1","1","0","607","0","1","2019-07-12 15:03:13","2019-07-12 15:03:13");
INSERT INTO room_master VALUES("80","1","1","1","0","608","0","1","2019-07-12 15:03:20","2019-07-12 15:03:20");
INSERT INTO room_master VALUES("81","1","1","1","0","609","0","1","2019-07-12 15:03:29","2019-07-12 15:03:29");
INSERT INTO room_master VALUES("82","1","1","1","0","610","0","1","2019-07-12 15:03:36","2019-07-12 15:03:36");
INSERT INTO room_master VALUES("83","1","1","1","0","611","0","1","2019-07-12 15:03:42","2019-07-12 15:03:42");
INSERT INTO room_master VALUES("84","1","1","1","0","612","0","1","2019-07-12 15:03:50","2019-07-12 15:03:50");
INSERT INTO room_master VALUES("85","1","1","1","0","613","0","1","2019-07-12 15:03:56","2019-07-12 15:03:56");
INSERT INTO room_master VALUES("86","1","1","1","0","614","0","1","2019-07-12 15:04:03","2019-07-12 15:04:03");
INSERT INTO room_master VALUES("87","1","1","1","0","615","0","1","2019-07-12 15:04:10","2019-07-12 15:04:10");
INSERT INTO room_master VALUES("88","1","1","1","0","616","0","1","2019-07-12 15:04:15","2019-07-12 15:04:15");
INSERT INTO room_master VALUES("89","1","1","1","0","617","0","1","2019-07-12 15:04:21","2019-07-12 15:04:21");
INSERT INTO room_master VALUES("90","1","1","1","0","618","0","1","2019-07-12 15:04:28","2019-07-12 15:04:28");
INSERT INTO room_master VALUES("91","1","1","1","0","619","0","1","2019-07-12 15:04:36","2019-07-12 15:04:36");
INSERT INTO room_master VALUES("92","1","1","2","0","701","0","1","2019-07-12 15:05:03","2019-07-12 15:05:03");
INSERT INTO room_master VALUES("93","1","1","2","0","702","0","1","2019-07-12 15:05:11","2019-07-12 15:05:11");
INSERT INTO room_master VALUES("94","1","1","2","0","703","0","1","2019-07-12 15:05:17","2019-07-12 15:05:17");
INSERT INTO room_master VALUES("95","1","1","2","0","704","0","1","2019-07-12 15:05:24","2019-07-12 15:05:24");
INSERT INTO room_master VALUES("96","1","1","2","0","705","0","1","2019-07-12 15:05:36","2019-07-12 15:05:36");
INSERT INTO room_master VALUES("97","1","1","1","0","707","0","1","2019-07-12 15:05:52","2019-07-12 15:05:52");
INSERT INTO room_master VALUES("98","1","1","1","0","708","0","1","2019-07-12 15:05:58","2019-07-12 15:05:58");
INSERT INTO room_master VALUES("99","1","1","1","0","709","0","1","2019-07-12 15:06:09","2019-07-12 15:06:09");





CREATE TABLE `room_size` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `size` varchar(255) NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1' COMMENT '0-Incactive,1-Active',
  `date_time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO room_size VALUES("1","Single","1","2019-06-04 13:08:05");
INSERT INTO room_size VALUES("2","Double","1","2019-06-04 13:08:05");
INSERT INTO room_size VALUES("3","Sweet","1","2019-06-06 15:38:43");





CREATE TABLE `room_type` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `room_type` varchar(255) NOT NULL,
  `status` int(100) NOT NULL DEFAULT '1' COMMENT '0-Incactive,1-Active',
  `date_time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO room_type VALUES("1","Standard","1","2019-06-04 13:08:05");





CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `tmp_type` int(11) DEFAULT NULL,
  `default_temp` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO templates VALUES("1","jet_tmp","1","0","1");
INSERT INTO templates VALUES("2","api","0","0","1");
INSERT INTO templates VALUES("7","kartiano_web","1","1","1");
INSERT INTO templates VALUES("8","fresh_framework","0","1","1");





CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `scheme_id` varchar(255) DEFAULT NULL COMMENT 'department Wise',
  `date` varchar(255) DEFAULT NULL,
  `utype` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO users VALUES("1","admin","admin","Admin","9411950511","admin@gmail.com","","01/02/2017","Administrator","1");
INSERT INTO users VALUES("2","EMP001","admin","Nagesh rai","8382936646","ad@gmail.com","2","","Employee","1");
INSERT INTO users VALUES("3","EMP003","admin","Uday","8188886786","","","2018-08-01","Employee","1");
INSERT INTO users VALUES("4","EMP0000004","","fdghf","gfdgfdg","dfgfdg","1","2019-05-31 07:10:29","Employee","1");



