<?php session_start();?>
<aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
   <div class="background_layer"></div>
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <div class="user-panel" style="padding-bottom: 60px;">
         <div class="pull-left image">
            <img src="images/user2-160x160.jpg" class="img-circle" alt="User Image"></a>
         </div>
         <div class="pull-left info" >
            <h3 style="margin:unset;"><?php echo $_SESSION['admin_name']; ?></h3>
            <a href="index.php?control=user&task=updateprofile&aid=<?php echo $_SESSION['adminid'];?>" title="Update Profile"><i class="fa fa-circle text-success"></i><b>Update Profile</b></a></br>
           <!--  <a href="index.php?control=user&task=changepassword" title="Change Password">
            <i class="fa fa-circle text-success"></i><b>Change Password</b></a>&nbsp;<br />
            <a class="mobile_logout" href="logout.php" title="Logout">
            <i class="fa fa-circle text-success"></i><b>Logout</b></a><br /> -->
         </div>
      </div>
      <?php if($_SESSION['utype']=='Administrator'){?>
      <ul class="sidebar-menu">
         <li class="treeview <?php if($_REQUEST['control']=='booking' && $_REQUEST['loc']=='1'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=booking&task=roomStatus&loc=1">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='branch_master' || $_REQUEST['control']=='room_master'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Master</span>
            </a>
            <ul class="treeview-menu">
               <!--  <li <?php if($_REQUEST['control']=='branch_master' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=branch_master&task=addnew">Add Branch</a></li>
                  <li <?php if($_REQUEST['control']=='branch_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=branch_master&task=show">View Branch</a></li> -->
               <!-- <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='addnew_type'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=addnew_type">Add Room Type</a></li> -->
               <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='show_type'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=show_type">View Room Type</a></li>
               <!-- <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='addnew_size'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=addnew_size">Add Room Size</a></li> -->
               <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='show_size'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=show_size">View Room Size</a></li>
               <!-- <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=addnew">Add Room</a></li> -->
               <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=show">View Room Master</a></li>
               <!-- <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='addnew_rate'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=addnew_rate">Add Rate</a></li> -->
               <li <?php if($_REQUEST['control']=='room_master' && $_REQUEST['task']=='show_rate'){ ?>class="active"<?php } ?>><a href="index.php?control=room_master&task=show_rate">View Rate</a></li>
            </ul>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='booking' && $_REQUEST['loc']=='' ){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-bed" aria-hidden="true"></i><span>Booking</span>
            </a>
            <ul class="treeview-menu">
               <!-- <li <?php if($_REQUEST['control']=='booking' && $_REQUEST['task']=='roomStatus'){ ?>class="active"<?php } ?>><a href="index.php?control=booking&task=roomStatus">New Booking</a></li> -->
               <li <?php if($_REQUEST['control']=='booking' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=booking&task=show">View All</a></li>
               <li <?php if($_REQUEST['control']=='booking' && $_REQUEST['task']=='show_guest'){ ?>class="active"<?php } ?>><a href="index.php?control=booking&task=show_guest">Guest History</a></li>
               <!-- <li <?php if($_REQUEST['control']=='booking' && $_REQUEST['task']=='category'){ ?>class="active"<?php } ?>><a href="index.php?control=booking&task=category">Category</a></li> -->
            </ul>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='report'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i><span>Report</span>
            </a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">Sale Amount</a></li>
               <!-- <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show_guest'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show_guest">All Guest</a></li> -->
            </ul>
         </li>
          <!-- <li class="treeview">
            <a href="logout.php">
            <i class="fa fa-sign-out" aria-hidden="true"></i>
            <span>Logout</span>
            </a>
         </li> -->
      </ul>
      <?php } ?>
      <?php if($_SESSION['utype']=='Employee'){?>
      <ul class="sidebar-menu">
         <li <?php if($_REQUEST['control']=='user'){ ?>class="active"<?php } ?>><a href="index.php?control=user"><i class="fa fa-user" aria-hidden="true"></i>Employee Details</a></li>
      </ul>
      <?php } ?>
   </section>
   <!-- /.sidebar -->
</aside>
<script type="text/javascript">
/*$(document).ready(function () {
   $(".sidebar-menu li:has('ul')").hover(function () {
         $(".treeview-menu").stop().slideDown();
      }, function () {
         $(".treeview-menu").hide();
   });
});*/
</script>
