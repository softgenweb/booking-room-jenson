<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class masterClass extends DbAccess {
		public $view='';
		public $name='master';
		
		
		/***************************************************** Country START **********************************************************/
		
		function show(){	
		$uquery ="select * from scheme where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row=count($uresults);	
		$_SESSION['country'] = $uquery;
		$tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
			
		}
			
		function addnew(){				
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM scheme WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}
		
		function save(){
		
			$name = mysql_real_escape_string($_POST['name']);	
			$status = '1';
			$date = date('Y-m-d');
			$id  = $_REQUEST['id'];
		
			$users = mysql_num_rows(mysql_query("select * from scheme where name='".$name."'"));
			
			if($users == '0') {
				
					if(!$id){
						
						$query = "INSERT INTO `scheme`(`name`,`status`,`date`) VALUES ('".$name."','1','".$date."')";
						$this->Query($query);
						$this->Execute();	
								
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=master&task=addnew");
					      }
					}
					else {
					$_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;
					header("location:index.php?control=master&task=addnew");
					} 
					
					if($id){
							$users = mysql_num_rows(mysql_query("select * from scheme where name='".$name."' and id!='".$id."'"));
			                if($users == '0') {
						         $update="UPDATE `scheme` SET `name`='".$name."' where id='".$id."'";		
						         $this->Query($update);
						        $this->Execute();
						//$_SESSION['msg'] = '1';
						   $_SESSION['alertmessage'] = UPDATERECORD; 
						   $_SESSION['errorclass'] = SUCCESSCLASS;
						  header("location:index.php?control=master&task=show");
							}
						else {
					      $_SESSION['alertmessage'] = DUPLICATE; 
					      $_SESSION['errorclass'] = ERRORCLASS;
					      header("location:index.php?control=master&task=addnew");
					    } 
						
					}
			
		/*	 $_SESSION['alertmessage'] = DUPLICATE; 
			   $_SESSION['errorclass'] = ERRORCLASS;
	     	header("location:index.php?control=staff&task=addnew");*/
		}
		
				
		
		function status(){
		$query="update scheme set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=master&task=show");
		}		
		
		function delete(){
		
		$query="DELETE FROM users WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';			
		$_SESSION['alertmessage'] = DELETE; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		
		header("location:index.php?control=staff&task=show");
		}
		
		
		
			
		function type(){	
		$uquery ="select * from type where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row=count($uresults);	
		$_SESSION['country'] = $uquery;
		$tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();			
		require_once("views/".$this->name."/".$this->task.".php"); 			
		}
		
			
		
		function type_status(){
		$query="update type set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=master&task=type");
		}
		
		
			
		
			
		function category(){	
		$uquery ="select * from category where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row=count($uresults);	
		$_SESSION['country'] = $uquery;
		$tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();			
		require_once("views/".$this->name."/".$this->task.".php"); 			
		}
		
			
		
		function category_status(){
		$query="update category set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=master&task=category");
		}
		
				
		function house(){	
		$uquery ="select * from category_house where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row=count($uresults);	
		$_SESSION['country'] = $uquery;
		$tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();			
		require_once("views/".$this->name."/".$this->task.".php"); 			
		}
		
			
		
		function house_status(){
		$query="update category_house set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=master&task=house");
		}
		
		
		
	
	
	}
