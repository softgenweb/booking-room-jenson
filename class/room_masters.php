<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class room_masterClass extends DbAccess {
		public $view='';
		public $name='room_master';
		
			
		function show(){
			$uquery = "SELECT * FROM `room_master` WHERE 1 ";
			$this->Query($uquery);
            $uresults               = $this->fetchArray();
            $no_of_row              = count($uresults);
            $tdata                  = count($uresults);
            /* Paging start here */
            $page                   = intval($_REQUEST['page']);
            $_REQUEST['tpages']     = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
            $adjacents              = intval($_REQUEST['adjacents']);
            $tdata                  = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
            $tdata                  = floor($tdata);
            if ($page <= 0)
                $page = 1;
            if ($adjacents <= 0)
                $tdata ? ($adjacents = 4) : 0;
            $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
            /* Paging end here */
            $query  = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
            $this->Query($query);
            $results = $this->fetchArray();
			require_once("views/" . $this->name . "/" . $this->task . ".php");
		}

		function addnew(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM room_master WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save(){

			$branch_id = $_REQUEST['branch_id'];			
			$room_type_id = $_REQUEST['room_type_id'];			
			$room_size_id = $_REQUEST['room_size_id'];			
			$capacity = $_REQUEST['capacity'];			
			$room_no = $_REQUEST['room_no'];			

			$id = $_REQUEST['id'];

			if(!$id){

				$query = "INSERT INTO `room_master`(`branch_id`, `room_type_id`, `room_size_id`, `capacity`, `room_no`, `date_created`, `date_modify`) VALUES ('".$branch_id."', '".$room_type_id."', '".$room_size_id."', '".$capacity."', '".$room_no."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";

				mysql_query($query);
				
				/*===================Activity Log====================*/
				$activity = "Add Room(".$room_no.") Created by ".$_SESSION['username'];

				$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
				/*===================================================*/
				
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show");

			}else{

				$query = "UPDATE `room_master` SET `branch_id`='".$branch_id."', `room_type_id`='".$room_type_id."', `room_size_id`='".$room_size_id."', `capacity`='".$capacity."', `room_no`='".$room_no."' WHERE `id`='".$id."'";

				mysql_query($query);
			
			/*===================Activity Log====================*/
			$activity = "Update Room(".$room_no.") Created by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 			

				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show");
			}

				
				

		}

		function status(){
			
			$query="UPDATE `room_master` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();

			/*===================Activity Log====================*/
			$activity = "Change Status of Room (".$_REQUEST['id'].") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			$this->task="show";
			$this->view ='show';
			//$this->show();	
			header("location:index.php?control=room_master&task=show");

		}

		function delete(){

		}
	/*--------------------------------Add Rate Master----------------------------*/
		function show_rate(){
			$uquery = "SELECT * FROM `rate_master` WHERE 1 ";
			$this->Query($uquery);
            $uresults               = $this->fetchArray();
            $no_of_row              = count($uresults);
            $tdata                  = count($uresults);
            /* Paging start here */
            $page                   = intval($_REQUEST['page']);
            $_REQUEST['tpages']     = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
            $adjacents              = intval($_REQUEST['adjacents']);
            $tdata                  = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
            $tdata                  = floor($tdata);
            if ($page <= 0)
                $page = 1;
            if ($adjacents <= 0)
                $tdata ? ($adjacents = 4) : 0;
            $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
            /* Paging end here */
            $query  = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
            $this->Query($query);
            $results = $this->fetchArray();
			require_once("views/" . $this->name . "/" . $this->task . ".php");
		}

		function addnew_rate(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM rate_master WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save_rate(){

					
			$room_type_id = $_REQUEST['room_type_id'];			
			$room_size_id = $_REQUEST['room_size_id'];			
			$stay_time = ($_REQUEST['stay_time']);			
			$type = ($_REQUEST['type']);			
			$rate = $_REQUEST['rate'];			
			$rate_code = strtoupper($_REQUEST['rate_code']);
			$remark = ucwords($_REQUEST['remark']);
			
			$id = $_REQUEST['id'];

			if(!$id){

				$query = "INSERT INTO `rate_master`(`room_type_id`, `room_size_id`, `stay_time`, `type`, `rate`, `rate_code`, `remark`,`date_created`) VALUES ('".$room_type_id."', '".$room_size_id."','".$stay_time."','".$type."', '".$rate."','".$rate_code."', '".$remark."','".date('Y-m-d H:i:s')."')";
				
				mysql_query($query);
				
				/*===================Activity Log====================*/
				$activity = "Add Rate Created by ".$_SESSION['username'];

				$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
				/*===================================================*/
				
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show_rate");

			}else{

				$query = "UPDATE `rate_master` SET `room_type_id`='".$room_type_id."', `room_size_id`='".$room_size_id."', `stay_time`='".$stay_time."', `type`='".$type."', `rate`='".$rate."', `rate_code`='".$rate_code."', `remark`='".$remark."', `date_modify`='".date('Y-m-d H:i:s')."' WHERE `id`='".$id."'";

				mysql_query($query);
			
			/*===================Activity Log====================*/
			$activity = "Update Rate Created by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 			

				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show_rate");
			}

				
				

		}
		
		function status_rate(){
		
			$query="UPDATE `rate_master` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();

			/*===================Activity Log====================*/
			$activity = "Change Status of Rate (".$_REQUEST['id'].") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			$this->task="show_rate";
			$this->view ='show_rate';
			//$this->show();	
			header("location:index.php?control=room_master&task=show_rate");

		}

/*--------------------------------Add Rate Master----------------------------*/

/*--------------------------------Add Room Type Master----------------------------*/

		function show_type(){
			$uquery = "SELECT * FROM `room_type` WHERE 1 ";
			$this->Query($uquery);
            $uresults               = $this->fetchArray();
            $no_of_row              = count($uresults);
            $tdata                  = count($uresults);
            /* Paging start here */
            $page                   = intval($_REQUEST['page']);
            $_REQUEST['tpages']     = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
            $adjacents              = intval($_REQUEST['adjacents']);
            $tdata                  = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
            $tdata                  = floor($tdata);
            if ($page <= 0)
                $page = 1;
            if ($adjacents <= 0)
                $tdata ? ($adjacents = 4) : 0;
            $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
            /* Paging end here */
            $query  = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
            $this->Query($query);
            $results = $this->fetchArray();
			require_once("views/" . $this->name . "/" . $this->task . ".php");
		}

		function addnew_type(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM room_type WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save_type(){
				
			$room_type = ucwords($_REQUEST['room_type']);			
						$id = $_REQUEST['id'];

			if(!$id){

				$query = "INSERT INTO `room_type`(`room_type`, `date_time`) VALUES ('".$room_type."','".date('Y-m-d H:i:s')."')";
				
				mysql_query($query);
				
				/*===================Activity Log====================*/
				$activity = "Add Type Created by ".$_SESSION['username'];

				$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
				/*===================================================*/
				
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show_type");

			}else{

				$query = "UPDATE `room_type` SET `room_type`='".$room_type."' WHERE `id`='".$id."'";

				mysql_query($query);
			
			/*===================Activity Log====================*/
			$activity = "Update Type Created by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 			

				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show_type");
			}

				
				

		}
		
		function status_type(){
		
			$query="UPDATE `room_type` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();

			/*===================Activity Log====================*/
			$activity = "Change Status of Type (".$_REQUEST['id'].") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			$this->task="show_type";
			$this->view ='show_type';
			//$this->show();	
			header("location:index.php?control=room_master&task=show_type");

		}

/*--------------------------------Add Room Type  Master----------------------------*/

/*--------------------------------Add Room Size Master----------------------------*/

		function show_size(){
			$uquery = "SELECT * FROM `room_size` WHERE 1 ";
			$this->Query($uquery);
            $uresults               = $this->fetchArray();
            $no_of_row              = count($uresults);
            $tdata                  = count($uresults);
            /* Paging start here */
            $page                   = intval($_REQUEST['page']);
            $_REQUEST['tpages']     = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
            $adjacents              = intval($_REQUEST['adjacents']);
            $tdata                  = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
            $tdata                  = floor($tdata);
            if ($page <= 0)
                $page = 1;
            if ($adjacents <= 0)
                $tdata ? ($adjacents = 4) : 0;
            $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
            /* Paging end here */
            $query  = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
            $this->Query($query);
            $results = $this->fetchArray();
			require_once("views/" . $this->name . "/" . $this->task . ".php");
		}

		function addnew_size(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM room_size WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save_size(){
				
			$size = ucwords($_REQUEST['size']);			
						$id = $_REQUEST['id'];

			if(!$id){

				$query = "INSERT INTO `room_size`(`size`, `date_time`) VALUES ('".$size."','".date('Y-m-d H:i:s')."')";
				
				mysql_query($query);
				
				/*===================Activity Log====================*/
				$activity = "Add Size Created by ".$_SESSION['username'];

				$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
				/*===================================================*/
				
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show_size");

			}else{

				$query = "UPDATE `room_size` SET `size`='".$size."' WHERE `id`='".$id."'";

				mysql_query($query);
			
			/*===================Activity Log====================*/
			$activity = "Update Size Created by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 			

				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=room_master&task=show_size");
			}

				
				

		}
		
		function status_size(){
		
			$query="UPDATE `room_size` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();

			/*===================Activity Log====================*/
			$activity = "Change Status of Size (".$_REQUEST['id'].") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			$this->task="show_size";
			$this->view ='show_size';
			//$this->show();	
			header("location:index.php?control=room_master&task=show_size");

		}

/*--------------------------------Add Room Size  Master----------------------------*/
	}
