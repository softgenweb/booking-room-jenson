<?php
require_once ('dbaccess.php');

require_once ('textconfig/config.php');

if (file_exists('configuration.php'))
	{
	require_once ('configuration.php');

	}

class db_backupClass extends DbAccess

	{
	public $view = '';

	public $name = 'db_backup';

	function show()
		{
		require_once ("views/" . $this->name . "/" . $this->task . ".php");

		}

	function dbdata_access()
		{
		$tables = '*';

		// get all of the tables

		if ($tables == '*')
			{
			$tables = array();
			$result = mysql_query('SHOW TABLES');
			while ($row = mysql_fetch_row($result))
				{
				$tables[] = $row[0];
				}
			}
		  else
			{
			$tables = is_array($tables) ? $tables : explode(',', $tables);
			}

		// cycle through

		foreach($tables as $table)
			{
			$result = mysql_query('SELECT * FROM ' . $table);
			$num_fields = mysql_num_fields($result);
			$return.= ''; //'DROP TABLE '.$table.';';
			$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
			$return.= "\n\n" . $row2[1] . ";\n\n";
			for ($i = 0; $i < $num_fields; $i++)
				{
				while ($row = mysql_fetch_row($result))
					{
					$return.= 'INSERT INTO ' . $table . ' VALUES(';
					for ($j = 0; $j < $num_fields; $j++)
						{
						$row[$j] = addslashes($row[$j]);
						$row[$j] = ereg_replace("\n", "\\n", $row[$j]);
						if (isset($row[$j]))
							{
							$return.= '"' . $row[$j] . '"';
							}
						  else
							{
							$return.= '""';
							}

						if ($j < ($num_fields - 1))
							{
							$return.= ',';
							}
						}

					$return.= ");\n";
					}
				}

			$return.= "\n\n\n";
			}

		// save file

		$handle = fopen('db_backup/restaurant-db-backup.sql', 'w+'); //for Folder in local xampp

		// $handle = fopen('db_backup/restaurant-db-backup-'.date('d-m-Y').'-'.date('h-i-s').'.sql','w+'); //for Folder in local xampp
		// $handle = fopen('E:/db/restaurant-db-backup-'.date('d-m-Y').'-'.time().'.sql','w+');  // Root file folder

		if ($handle > 0)
			{
			fwrite($handle, $return);
			fclose($handle);
			echo "<div class='alert alert-success alert-dismissable' style='margin:35px;height:70px;'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
    <strong>Success!</strong> Database backup Created Successfully..
  </div>";
			}
		  else
			{
			echo "<div class='alert alert-danger alert-dismissable' style='margin:35px;height:70px;'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
    <strong>Oops!</strong> Something problem to create Database Backup..
  </div>";
			}

		require_once ("views/" . $this->name . "/show.php");

		}

	function import_db()
		{
		require_once ("views/" . $this->name . "/" . $this->task . ".php");

		}

	function importdb_save()
		{

		// Name of the file

		$filename = $_FILES['docpicker']['name']; //'db_backup/restaurant-db-backup-19-01-2019-04-06-47.sql'; //$_FILES['docpicker'][];//

		// MySQL host

		/*$mysql_host = 'localhost';

		// MySQL username

		$mysql_username = 'root';

		// MySQL password

		$mysql_password = '';

		// Database name

		$mysql_database = 'users';

		// Connect to MySQL server

		mysql_connect($mysql_host, $mysql_username, $mysql_password) or die('Error connecting to MySQL server: ' . mysql_error());

		// Select database

		mysql_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());*/

		// Temporary variable, used to store current query

		include ('../configuration.php');

		$database_name = "db_restaurant";
		/* query all tables */
		$found_tables[] = '';
		$sql = "SHOW TABLES FROM $database_name";
		if ($result = mysql_query($sql))
			{
			/* add table name to array */
			while ($row = mysql_fetch_row($result))
				{
				$found_tables[] = $row[0];
				}
			}
		  else
			{
			die("Error, could not list tables. MySQL Error: " . mysql_error());
			}

		// loop through and drop each table

		foreach($found_tables as $table_name)
			{
			$sql = "DROP TABLE $database_name.$table_name";
			if ($result = mysql_query($sql))
				{

				// echo "<br />Success - table $table_name deleted.";

				}
			  else
				{

				// echo "<br />Error deleting ".$table_name." MySQL Error: " . mysql_error() . "";

				}
			}

		$templine = '';

		// Read in entire file

		$lines = file($filename);

		// Loop through each line

		foreach($lines as $line)
			{

			// Skip it if it's a comment

			if (substr($line, 0, 2) == '--' || $line == '') continue;

			// Add this line to the current segment

			$templine.= $line;

			// If it has a semicolon at the end, it's the end of the query

			if (substr(trim($line) , -1, 1) == ';')
				{

				// Perform the query

				mysql_query($templine) or print ('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');

				// Reset temp variable to empty

				$templine = '';
				}
			}

		$_SESSION['alertmessage'] = "Tables imported successfully"; //ADDNEWRECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;

		// echo "Tables imported successfully";

		require_once ("views/" . $this->name . "/show.php");

		}
	}

