<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class branch_masterClass extends DbAccess {
		public $view='';
		public $name='branch_master';
		
			
		function show(){
			$uquery = "SELECT * FROM `branch_master` WHERE 1 ";
			$this->Query($uquery);
            $uresults               = $this->fetchArray();
            $no_of_row              = count($uresults);
            $tdata                  = count($uresults);
            /* Paging start here */
            $page                   = intval($_REQUEST['page']);
            $_REQUEST['tpages']     = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE; //$tdata; // 20 by default
            $adjacents              = intval($_REQUEST['adjacents']);
            $tdata                  = ($tdata % $tpages) ? (($tdata / $tpages) + 1) : round($tdata / $tpages); //$_GET['tpages'];// 
            $tdata                  = floor($tdata);
            if ($page <= 0)
                $page = 1;
            if ($adjacents <= 0)
                $tdata ? ($adjacents = 4) : 0;
            $reload = $_SERVER['PHP_SELF'] . "?control=" . $_REQUEST['control'] . "&views=" . $_REQUEST['view'] . "&task=" . $_REQUEST['task'] . "&tmpid=" . $_REQUEST['tmpid'] . "&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
            /* Paging end here */
            $query  = $uquery . " LIMIT " . (($page - 1) * $tpages) . "," . $tpages;
            $this->Query($query);
            $results = $this->fetchArray();
			require_once("views/" . $this->name . "/" . $this->task . ".php");
		}

		function addnew(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM branch_master WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save(){
			$branch_name = $_REQUEST['branch_name'];			

			$id = $_REQUEST['id'];

			if(!$id){
				$query = "INSERT INTO `branch_master`( `branch_name`, `date_time`) VALUES ('".$branch_name."', '".date('Y-m-d H:i:s')."')";
				mysql_query($query);
				
			/*===================Activity Log====================*/
			$activity = "Add Branch Created by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
				
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=branch_master&task=show");
			}else{
				$query = "UPDATE `branch_master` SET `branch_name`='".$branch_name."' WHERE `id`='".$id."'";

				mysql_query($query);
			
			/*===================Activity Log====================*/
			$activity = "Update Branch Created by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 			

				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=branch_master&task=show");
			}

				
				

		}

		function status(){

		}

		function delete(){

		}
	
	
	}
